import os

import cx_Oracle as Oracle

# For Instant Oracle Installation and configuration, see the source
# https://cx-oracle.readthedocs.io/en/latest/user_guide/installation.html#installing-cx-oracle-on-macos-intel-x86
Oracle.init_oracle_client(lib_dir=os.getenv('CX_ORACLE_INSTANTORA_PATH'))


class Azprd(object):
    """Wrapper for cx_Oracle"""

    def __init__(self):
        self.connection = None
        self.user = None
        self.password = None
        self.host = None
        self.service_name = None
        self.host_serv_nm = None

    def load_credential(self):
        env_vars = {
            "user": "CX_ORACLE_USER",
            "password": "CX_ORACLE_PASS",
            "host": "CX_ORACLE_HOST",
            "service_name": "CX_ORACLE_SERVICE_NAME",
            "instantora_path": 'CX_ORACLE_INSTANTORA_PATH'
        }

        missing_env_vars = [key for key in env_vars if os.getenv(env_vars[key]) is None]

        if missing_env_vars:
            raise KeyError(f"Missing Environment Variables: [{', '.join(missing_env_vars)}]")

        self.user = os.getenv(env_vars['user'])
        self.password = os.getenv(env_vars['password'])
        self.host = os.getenv(env_vars['host'])
        self.user = os.getenv(env_vars['user'])
        self.service_name = os.getenv(env_vars['service_name'])
        self.host_serv_nm = f"{self.host}/{self.service_name}"

    def connect(self):
        self.load_credential()
        self.connection = Oracle.connect(self.user, self.password, self.host_serv_nm)

    def execute_sql(self, sql=""):
        cursor = self.connection.cursor()

        cursor.execute(sql)

        return cursor

    def disconnect(self):
        if self.connection:
            self.connection.close()
