import json as js
import logging
import multiprocessing as mp
import os
import shutil
import warnings
from datetime import datetime
from pathlib import Path

import tzlocal

from oracle_to_csv_multithread import execute_from_sql_file
from utils import progress_bar_with_time

warnings.filterwarnings("ignore")
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
line = None


def init_line(args):
    global line

    line = args


def current_datetime(add_time=False):
    utc = tzlocal.get_localzone()
    return datetime.now(tz=utc).strftime(f"%Y-%m-%d{' %H.%M.%S%z' if add_time else ''}")


def download_azprd(query_data):
    """
    Receives in `query_data` a structure like:
    {
      "sql_file": "query_files/CommerceAEs.sql",
      "filepath": "~/Git/Digital360/AZPRD/CommerceAEs.csv",
      "status": "inactive"
    }
    """

    global line

    home = str(Path.home())
    filepath = query_data['sql_file'].replace('~', home)
    filedest = query_data['filepath'].replace('~', home)
    status = query_data['status']

    if status == 'active':
        with line.get_lock():
            value = line.value
            line.value += 1

        with progress_bar_with_time(prefix=f'Dowloading {filepath}', line=value, clear=False):
            shutil.move(execute_from_sql_file(filepath=filepath), filedest)


if __name__ == "__main__":
    metadata_file = open('input/azprd-to-digital360.json')
    metadata = js.load(metadata_file)

    # Gets metadata for building the dataset
    extended = metadata['org62-reports-joined-path'].replace('<x>', current_datetime())
    extended = os.path.join(os.path.dirname(__file__), extended)
    final_csv_path = metadata['output-csv-path'].replace('<x>', current_datetime(add_time=True))
    azprd_sql_files = metadata['azprd-sql-files']

    # Initializes a Pool for downloading from azprd.
    pool_size = len(azprd_sql_files)
    line = mp.Value('i', 0)
    with mp.Pool(processes=pool_size, initializer=init_line, initargs=(line,)) as p:
        p.map(download_azprd, azprd_sql_files)

    p.join()

    # Closes files
    metadata_file.close()
