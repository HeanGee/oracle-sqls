import csv
import os
import re
from pathlib import Path

from MyOracle import Azprd


def relative_mkdir(name="", as_pymodule=True, abs=False):
    if name:
        Path(name).mkdir(parents=True, exist_ok=True)
        init_file_path = f"{os.getcwd() if not abs else ''}/{name}/{'__init__.py' if as_pymodule else ''}"
        if not (os.path.isfile(init_file_path) or os.path.isdir(init_file_path)):
            open(init_file_path, 'w').close()


class AzprdDriver(object):
    def __init__(self):
        self.connection = Azprd()

    def connect(self):
        self.connection.connect()

    def disconnect(self):
        if self.connection:
            self.connection.disconnect()
            self.connection = None

    def export_to_csv(self, query_string=None, file_name="Exported_From_Oracle"):
        # Request query
        cursor = self.connection.execute_sql(query_string)

        # Prepare file
        relative_mkdir(name="generated", as_pymodule=False, abs=False)
        csv_file_dest = os.getcwd() + f"/generated/{file_name}.csv"
        output_file = open(csv_file_dest, 'w')  # 'wb'
        output = csv.writer(output_file, dialect='excel')

        # Write Headers
        cols = []
        for col in cursor.description:
            cols.append(col[0])
        output.writerow(cols)

        # Write data rows
        rows = []
        for row_data in cursor:
            rows.append(row_data)

        output.writerows(rows)

        output_file.close()

        return csv_file_dest


def execute_from_sql_file(filepath=""):
    # Get only the file name.
    filename = filepath.split(sep=os.path.sep).pop()

    # Query Oracle Server.
    oracle = AzprdDriver()
    oracle.connect()
    with open(filepath, 'r') as f:
        full_sql = f.read()
        filename = re.sub(".sql", "", filename)
        abspath = oracle.export_to_csv(query_string=full_sql, file_name=filename)

    print()
    return abspath
