import os
import unittest

import pandas as pd

from . import PlacematTest

TESTDATA_DATASET = os.path.join(os.path.dirname(__file__), 'csv/fake_expected_accounts_fixed.csv')
TESTDATA_EXPECTED_DATASET = os.path.join(os.path.dirname(__file__), 'csv/fake_expected_dataset_generated.csv')


class TestDatasetBuilder(unittest.TestCase):
    def setUp(self) -> None:
        """For the moment no initialization is needed."""

        pass

    def test_dataset_build(self):
        dataset = pd.read_csv(TESTDATA_DATASET)

        placemat = PlacematTest.Test(dataset)
        dataset = placemat.generate_plays()
        expected_dataset = pd.read_csv(TESTDATA_EXPECTED_DATASET)
        print(dataset.dtypes)
        print(expected_dataset.dtypes)
        # for index, row in dataset.iterrows():
        #     print(f"|{row['Test Play Test 01 ACV']}|")
        # for index, row in expected_dataset.iterrows():
        #     print(f"|{row['Test Play Test 01 ACV']}|")

        # assert_frame_equal(dataset, expected_dataset)

        # for ((i, j), (k, l)) in zip(dataset.iterrows(), expected_dataset.iterrows()):
        #     print(f"|{j['Test Play Test 01 ACV']}| == |{l['Test Play Test 01 ACV']}|")
        #     print(j['Test Play Test 01 ACV'] == l['Test Play Test 01 ACV'])
        #     # self.assertTrue(j['Test Play Test 01 ACV'] == l['Test Play Test 01 ACV'])
        # self.assertTrue(dataset[['Test Play Test 01 ACV']].equals(expected_dataset[['Test Play Test 01 ACV']]))
