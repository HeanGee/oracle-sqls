import json as js
import os
import unittest

import pandas as pd

from account_detail_dups_fixer import fix_account_details, fix_detail

TESTDATA_EXPECTED_DETAILS = os.path.join(os.path.dirname(__file__), 'csv/fake_expected_accounts_fixed.csv')
TESTDATA_FIXER_INPUT = os.path.join(os.path.dirname(__file__), 'input/fake_account_detail_fixer_inputs.json')
TESTDATA_DETAILS = os.path.join(os.path.dirname(__file__), 'csv/fake_accounts_with_multiple_details.csv')
TESTDATA_REFERENCE_DETAILS = os.path.join(os.path.dirname(__file__), 'csv/fake_correct_accounts.csv')


class TestUtils(unittest.TestCase):
    def setUp(self):
        pass

    def test_account_fixer(self):
        json_file = open(TESTDATA_FIXER_INPUT)
        json = js.load(json_file)
        new, _ = fix_account_details(json)
        expected = pd.read_csv(TESTDATA_EXPECTED_DETAILS)
        json_file.close()
        self.assertTrue(new.equals(expected))

    def test_account_fixer2(self):
        to_fix = pd.read_csv(TESTDATA_DETAILS)
        fix_with = pd.read_csv(TESTDATA_REFERENCE_DETAILS)
        expected = pd.read_csv(TESTDATA_EXPECTED_DETAILS)
        result = fix_detail(to_fix=to_fix, fix_with=fix_with)
        self.assertTrue(result.equals(expected))
