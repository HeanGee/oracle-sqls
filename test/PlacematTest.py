from typing import Union

import pandas as pd

import utils


class Test:

    def __init__(self, df: pd.DataFrame):
        self.df = df

    def generate_plays(self):
        self.df = utils.set_output_fields_v3(self.df, 'Test Play Test 01')
        self.df = utils.set_output_fields_v3(self.df, 'Test Play Test 01 ACV')

        self.updates_play_fields()

        return self.df

    def updates_play_fields(self):
        self.df['Test Play Test 01'] = self.df.apply(play1, axis=1, which='play')
        self.df['Test Play Test 01 ACV'] = self.df.apply(play1, axis=1, which='acv')


# Play Test 01
def play1(df, which: str = None) -> Union[str, int, float]:
    play = "No"
    play_acv = 0
    a = 0 if pd.isna(df['A']) else int(df['A'])
    b = 0 if pd.isna(df['B']) else int(df['B'])

    if a < 5 and b == 20:
        play = "Yes"
        play_acv = "Row is SECOND"

    return play if which == 'play' else play_acv
