import csv
import os
import unittest

import pandas as pd

import utils

TESTDATA_ACCTINT = os.path.join(os.path.dirname(__file__), 'csv/fake_csv_file.csv')
TESTDATA_HLSINFO = os.path.join(os.path.dirname(__file__), 'csv/fake_hls.csv')


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.fake_token_list = ['ORDER', 'Status', 'lightning', 'qUantiTY', 'pEdRO']
        self.expected_token_list = ['ord', 'status', 'light', 'qty', 'pedr']
        self.non_expected_token_list = ['Ord', 'Status', 'Light', 'Qty', 'pEdR']
        self.play_field = 'Play Field Name'
        self.play = 'Yes'
        self.play_acv_field = 'ACV Field Name'
        self.play_acv = 1000
        self.hls_fields = ["hls1", "hls2"]
        self.hls_invalid_fields = ["hls3"]

    def test_set_output_fields(self):
        #  Good scenario
        with open(TESTDATA_ACCTINT, 'r+') as f:
            csv_file = csv.DictReader(f)
            headers = csv_file.fieldnames
            for row in csv_file:
                utils.set_output_fields(self.play_field, self.play, self.play_acv_field, self.play_acv, row, headers)

            self.assertEqual(self.play_field in headers, True)
            self.assertEqual(self.play_acv_field in headers, True)
            self.assertEqual(self.play, row[self.play_field])
            self.assertEqual(self.play_acv, row[self.play_acv_field])

    def test_set_output_fields_v2(self):
        csv_file = pd.read_csv(TESTDATA_ACCTINT)
        extension = {}
        rows = len(csv_file.index)

        for i, row in csv_file.iterrows():
            if i == 1:
                utils.set_output_fields_v2(row, rows, extension, self.play_field, self.play, i)
                utils.set_output_fields_v2(row, rows, extension, self.play_acv_field, self.play_acv, i)

        expected = {f'{self.play_field}': ["", f'{self.play}', ""], f'{self.play_acv_field}': ["", self.play_acv, ""]}

        self.assertEqual(extension == expected, True)

    def test_add_fields_from_hls(self):
        hls = csv.DictReader(open(TESTDATA_HLSINFO, encoding='utf-8-sig'))
        act = csv.DictReader(open(TESTDATA_ACCTINT, encoding='utf-8-sig'))

        HLSData = []
        HLSKey = {}
        rownumber = 0
        for row in hls:
            HLSKey[row['a'][:15]] = rownumber
            HLSData.append(row)
            rownumber += 1

        # Good scenario
        for row in act:
            AccountID = row['a']

            try:
                HLSRow = HLSData[HLSKey[AccountID]]
            except KeyError:
                HLSRow = None

            # Asserts that hlsrow is not empty
            self.assertIsNotNone(HLSRow)

            # Adds field from hls
            utils.add_fields_from_hls(HLSRow, row, act.fieldnames, self.hls_fields)

            # Asserts for each hls field that the field and value was added into account
            [self.assertEqual(row[self.hls_fields[i]], HLSRow[self.hls_fields[i]]) for i in range(len(self.hls_fields))]

        # Asserts that all fields was added in the header of account
        self.assertEqual(all([field in act.fieldnames for field in self.hls_fields]), True)

        # Bad Scenario: Tries to add a field not in hls.
        for row in act:
            AccountID = row['a']

            try:
                HLSRow = HLSData[HLSKey[AccountID]]
            except KeyError:
                HLSRow = None

            # Asserts that hlsrow is not empty
            self.assertIsNotNone(HLSRow)

            # Adds field from hls
            with self.assertRaises(Exception):
                utils.add_fields_from_hls(HLSRow, row, act.fieldnames, self.hls_fields)
