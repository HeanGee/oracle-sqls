import numpy as np
import pandas as pd
import random
import threading
import traceback
from queue import Empty, Queue
from typing import Union, List
from utils import calculate_segment, limit_value, progress_bar_with_time

random.seed()


class FY22_Commerce(threading.Thread):
    thid = -1

    AMOUNT_OF_PLAYS = 5

    FIELDNAMES = ['B2B Commerce Order Quantity', 'B2B Commerce PTB Segment (Numeric)', 'B2C Commerce  Segment(Numeric)',
                  'B2C Commerce Order Quantity', 'Core Service Order Quantity', 'Customer Communities Order Quantity',
                  'FY22-Comm Going Direct to Consumer', 'FY22-Comm Going Direct to Consumer ACV', 'FY22-Comm OMS',
                  'FY22-Comm OMS ACV', 'FY22-Comm Spare Parts & Aftermarket', 'FY22-Comm Spare Parts & Aftermarket ACV',
                  'FY22-Comm Transform & Grow Local Markets', 'FY22-Comm Transform & Grow Local Markets ACV',
                  'FY22-Comm Transform Manufacturing with B2C Commerce',
                  'FY22-Comm Transform Manufacturing with B2C Commerce ACV', 'Industry', 'New Logos',
                  'Order Management Order Quantity', 'Order Management PTB Segment (Numeric)', 'Sector', 'my-segment']

    PLAY_FIELDS = ['FY22-Comm Going Direct to Consumer', 'FY22-Comm OMS', 'FY22-Comm Spare Parts & Aftermarket', 'FY22-Comm Transform & Grow Local Markets', 'FY22-Comm Transform Manufacturing with B2C Commerce']

    ACVS_FIELDS = ['FY22-Comm Going Direct to Consumer ACV', 'FY22-Comm OMS ACV', 'FY22-Comm Spare Parts & Aftermarket ACV', 'FY22-Comm Transform & Grow Local Markets ACV', 'FY22-Comm Transform Manufacturing with B2C Commerce ACV']

    PLACEMAT_FIELDS = ['FY22-Comm Going Direct to Consumer', 'FY22-Comm Going Direct to Consumer ACV', 'FY22-Comm OMS', 'FY22-Comm OMS ACV', 'FY22-Comm Spare Parts & Aftermarket', 'FY22-Comm Spare Parts & Aftermarket ACV', 'FY22-Comm Transform & Grow Local Markets', 'FY22-Comm Transform & Grow Local Markets ACV', 'FY22-Comm Transform Manufacturing with B2C Commerce', 'FY22-Comm Transform Manufacturing with B2C Commerce ACV']

    OPERATION_FIELDS = ['B2B Commerce Order Quantity', 'B2B Commerce PTB Segment (Numeric)', 'B2C Commerce  Segment(Numeric)', 'B2C Commerce Order Quantity', 'Core Service Order Quantity', 'Customer Communities Order Quantity', 'Industry', 'New Logos', 'Order Management Order Quantity', 'Order Management PTB Segment (Numeric)', 'Sector', 'my-segment']

    MAP_PLAY_MTH = {'FY22-Comm Spare Parts & Aftermarket': 'play1', 'FY22-Comm Transform & Grow Local Markets': 'play2', 'FY22-Comm Going Direct to Consumer': 'play3', 'FY22-Comm OMS': 'play4', 'FY22-Comm Transform Manufacturing with B2C Commerce': 'play5'}

    EXTRA_FIELDS = {}

    def __init__(self, df: pd.DataFrame, reset_th_id=False, line: int = 0, event: threading.Event = None):
        super().__init__()

        self.id = self.__generate_thread_id(reset_th_id=reset_th_id)

        self.df = df
        self.line = line
        self.event = event
        self.name = 'FY22_Commerce'
        self.queue = Queue()
        self.workers: List[PlayCrawler] = []

    def __reset_th_id(self):
        self.__class__.thid = -1

    def __generate_thread_id(self, reset_th_id=False):
        self.__reset_th_id() if reset_th_id else None
        self.__increment_thid()
        return self.__class__.thid

    def __increment_thid(self):
        self.thid += 1

    def get_th_id(self):
        return self.id

    def run(self):
        # self.df = extender(df=self.df, line=self.line, event=self.event, queue=self.queue)

        for playname in FY22_Commerce.PLAY_FIELDS:  # One thread for each play
            worker = PlayCrawler(queue=self.queue, event=self.event)
            worker.daemon = True
            worker.start()

            self.workers.append(worker)

            funct_nm = FY22_Commerce.MAP_PLAY_MTH[playname]
            self.queue.put((self.df, globals()[funct_nm], playname))

        # Wait the thread until all items is consumed.
        self.queue.join()

        return self.df


class PlayCrawler(threading.Thread):
    def __init__(self, queue: Queue, event: threading.Event, name: str = ''):
        threading.Thread.__init__(self)
        self.event = event
        self.queue = queue
        self.name = f'CRAWLER-{name or random.randrange(1, 11, 1)}'
        self.exception = None
        self.id = self.name

    def run(self):
        while True:
            # Get the work from the queue and expand the n-ple
            df, function, play = self.queue.get()
            self.name = play

            try:
                if self.event.is_set():
                    function(df, event=self.event)
            except Exception as e:
                self.exception = e
                with open(f'generated/[log] {self.name}.txt', 'a') as f:
                    f.write(str(e))
                    f.write(traceback.format_exc())
            finally:
                self.queue.task_done()


# Spare Parts & Aftermarket
def play1(df, event: threading.Event = None):
    if not event.is_set():
        return

    b2b_comm_ptb_segm_num = df['B2B Commerce PTB Segment (Numeric)']
    b2b_comm_ord_qty = df['B2B Commerce Order Quantity']
    industry = df['Industry']
    my_segm = df['my-segment']

    play_cond = (b2b_comm_ptb_segm_num >= 4) & (b2b_comm_ord_qty == 0) & (
                industry == "Manufacturing, Automotive & Energy")

    df["FY22-Comm Spare Parts & Aftermarket"] = "No"
    df.loc[play_cond, 'FY22-Comm Spare Parts & Aftermarket'] = 'Yes'

    df['FY22-Comm Spare Parts & Aftermarket ACV'] = 0
    df.loc[play_cond & (my_segm == "ENTR"), "FY22-Comm Spare Parts & Aftermarket ACV"] = 94000
    df.loc[play_cond & (my_segm == "CMRCL"), "FY22-Comm Spare Parts & Aftermarket ACV"] = 45000


# Transform & Grow Local Markets
def play2(df, event: threading.Event = None):
    if not event.is_set():
        return

    b2b_comm_ptb_segm_num = df['B2B Commerce PTB Segment (Numeric)']
    b2b_comm_ord_qty = df['B2B Commerce Order Quantity']
    industry = df['Industry']
    sector = df['Sector']
    my_segm = df['my-segment']

    play_cond = (b2b_comm_ptb_segm_num >= 4) & (b2b_comm_ord_qty == 0) & (
                (industry.isin(["Manufacturing, Automotive & Energy", "High Tech"])) | (
            sector.isin(["Consumer Products", "Medical Devices and Diagnostics"])))

    df["FY22-Comm Transform & Grow Local Markets"] = "No"
    df.loc[play_cond, 'FY22-Comm Transform & Grow Local Markets'] = 'Yes'

    df['FY22-Comm Transform & Grow Local Markets ACV'] = 0
    df.loc[play_cond & (my_segm == "ENTR"), "FY22-Comm Transform & Grow Local Markets ACV"] = 94000
    df.loc[play_cond & (my_segm == "CMRCL"), "FY22-Comm Transform & Grow Local Markets ACV"] = 45000


# Going Direct to Consumer
def play3(df, event: threading.Event = None):
    if not event.is_set():
        return

    core_serv_ord_qty = df['Core Service Order Quantity']
    cust_comms_ord_qty = df['Customer Communities Order Quantity']
    b2c_comm_segm_num = df['B2C Commerce  Segment(Numeric)']

    play_cond = (core_serv_ord_qty > 0) & (cust_comms_ord_qty > 0) & (b2c_comm_segm_num >= 4)

    df["FY22-Comm Going Direct to Consumer"] = "No"
    df.loc[play_cond, 'FY22-Comm Going Direct to Consumer'] = 'Yes'

    df['FY22-Comm Going Direct to Consumer ACV'] = 0
    df.loc[play_cond, "FY22-Comm Going Direct to Consumer ACV"] = 250000


# OMS
def play4(df, event: threading.Event = None):
    if not event.is_set():
        return

    core_serv_ord_qty = df['Core Service Order Quantity']
    b2c_comm_ord_qty = df['B2C Commerce Order Quantity']
    ord_mana_ptb_segm_num = df['Order Management PTB Segment (Numeric)']
    ord_mana_ord_qty = df['Order Management Order Quantity']

    play_cond = (core_serv_ord_qty > 0) & (b2c_comm_ord_qty > 0) & (ord_mana_ptb_segm_num >= 4) & (
                ord_mana_ord_qty == 0)

    df["FY22-Comm OMS"] = "No"
    df.loc[play_cond, 'FY22-Comm OMS'] = 'Yes'

    df['FY22-Comm OMS ACV'] = 0
    df.loc[play_cond, "FY22-Comm OMS ACV"] = 250000


# Transform Manufacturing with B2C Commerce
def play5(df, event: threading.Event = None):
    if not event.is_set():
        return

    new_logo = df['New Logos']
    b2c_comm_segm_num = df['B2C Commerce  Segment(Numeric)']
    b2c_comm_ord_qty = df['B2C Commerce Order Quantity']
    industry = df['Industry']

    play_cond = (new_logo == 'left_only') & (b2c_comm_segm_num >= 4) & (b2c_comm_ord_qty == 0) & (
                industry == "Manufacturing, Automotive & Energy")

    df["FY22-Comm Transform Manufacturing with B2C Commerce"] = "No"
    df.loc[play_cond, 'FY22-Comm Transform Manufacturing with B2C Commerce'] = 'Yes'

    df['FY22-Comm Transform Manufacturing with B2C Commerce ACV'] = 0
    df.loc[play_cond, "FY22-Comm Transform Manufacturing with B2C Commerce ACV"] = 250000
