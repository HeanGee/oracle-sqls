import numpy as np
import pandas as pd
import random
import threading
import traceback
from queue import Empty, Queue
from typing import Union, List
from utils import calculate_segment, limit_value, progress_bar_with_time
random.seed()


class FY22_Marketing(threading.Thread):
    thid = -1

    AMOUNT_OF_PLAYS = 5

    FIELDNAMES = ['# Contacts', 'Account Detail Lookup: Account Details Name', 'Account Intelligence: Account Intelligence Name', 'Account Owner Role', 'Advertising Studio Order Quantity', 'Advertising Studio Segment (Numeric)', 'Core Org Status', 'Datorama Order Qty', 'Datorama Segment (Numeric)', 'FY22-Mktg Account-Based Marketing', 'FY22-Mktg Account-Based Marketing ACV', 'FY22-Mktg Customer-Centric Journeys', 'FY22-Mktg Customer-Centric Journeys ACV', 'FY22-Mktg Data-Driven Advertising', 'FY22-Mktg Data-Driven Advertising ACV', 'FY22-Mktg Real-Time Personalization', 'FY22-Mktg Real-Time Personalization ACV', 'FY22-Mktg Unify Your Data (CDP)', 'FY22-Mktg Unify Your Data (CDP) ACV', 'Interaction Studio Order Qty', 'Interaction Studio PTB Segment (Num)', 'Master Carve', 'Messaging Journeys Order Quantity', 'Messaging Journeys Segment (Numeric)', 'Pardot Order Quantity', 'Pardot PTB Segment (Numeric)', 'Region.']

    PLAY_FIELDS = ['FY22-Mktg Account-Based Marketing', 'FY22-Mktg Customer-Centric Journeys', 'FY22-Mktg Data-Driven Advertising', 'FY22-Mktg Real-Time Personalization', 'FY22-Mktg Unify Your Data (CDP)']

    ACVS_FIELDS = ['FY22-Mktg Account-Based Marketing ACV', 'FY22-Mktg Customer-Centric Journeys ACV', 'FY22-Mktg Data-Driven Advertising ACV', 'FY22-Mktg Real-Time Personalization ACV', 'FY22-Mktg Unify Your Data (CDP) ACV']

    PLACEMAT_FIELDS = ['FY22-Mktg Account-Based Marketing', 'FY22-Mktg Account-Based Marketing ACV', 'FY22-Mktg Customer-Centric Journeys', 'FY22-Mktg Customer-Centric Journeys ACV', 'FY22-Mktg Data-Driven Advertising', 'FY22-Mktg Data-Driven Advertising ACV', 'FY22-Mktg Real-Time Personalization', 'FY22-Mktg Real-Time Personalization ACV', 'FY22-Mktg Unify Your Data (CDP)', 'FY22-Mktg Unify Your Data (CDP) ACV']

    OPERATION_FIELDS = ['# Contacts', 'Account Detail Lookup: Account Details Name', 'Account Intelligence: Account Intelligence Name', 'Account Owner Role', 'Advertising Studio Order Quantity', 'Advertising Studio Segment (Numeric)', 'Core Org Status', 'Datorama Order Qty', 'Datorama Segment (Numeric)', 'Interaction Studio Order Qty', 'Interaction Studio PTB Segment (Num)', 'Master Carve', 'Messaging Journeys Order Quantity', 'Messaging Journeys Segment (Numeric)', 'Pardot Order Quantity', 'Pardot PTB Segment (Numeric)', 'Region.']

    MAP_PLAY_MTH = {'FY22-Mktg Customer-Centric Journeys': 'play1', 'FY22-Mktg Data-Driven Advertising': 'play2', 'FY22-Mktg Account-Based Marketing': 'play3', 'FY22-Mktg Real-Time Personalization': 'play4', 'FY22-Mktg Unify Your Data (CDP)': 'play5'}

    EXTRA_FIELDS = {}

    def __init__(self, df: pd.DataFrame, reset_th_id=False, line: int = 0, event: threading.Event = None):
        super().__init__()

        self.id = self.__generate_thread_id(reset_th_id=reset_th_id)

        self.df = df
        self.line = line
        self.event = event
        self.name = 'FY22_Marketing'
        self.queue = Queue()
        self.workers: List[PlayCrawler] = []

    def __reset_th_id(self):
        self.__class__.thid = -1

    def __generate_thread_id(self, reset_th_id=False):
        self.__reset_th_id() if reset_th_id else None
        self.__increment_thid()
        return self.__class__.thid

    def __increment_thid(self):
        self.thid += 1

    def get_th_id(self):
        return self.id

    def run(self):
        # self.df = extender(df=self.df, line=self.line, event=self.event, queue=self.queue)

        for playname in FY22_Marketing.PLAY_FIELDS:  # One thread for each play
            worker = PlayCrawler(queue=self.queue, event=self.event)
            worker.daemon = True
            worker.start()

            self.workers.append(worker)

            funct_nm = FY22_Marketing.MAP_PLAY_MTH[playname]
            self.queue.put((self.df, globals()[funct_nm], playname))

        # Wait the thread until all items is consumed.
        self.queue.join()

        return self.df


class PlayCrawler(threading.Thread):
    def __init__(self, queue: Queue, event: threading.Event, name: str = ''):
        threading.Thread.__init__(self)
        self.event = event
        self.queue = queue
        self.name = f'CRAWLER-{name or random.randrange(1, 11, 1)}'
        self.exception = None
        self.id = self.name

    def run(self):
        while True:
            # Get the work from the queue and expand the n-ple
            df, function, play = self.queue.get()
            self.name = play

            try:
                if self.event.is_set():
                    function(df, event=self.event)
            except Exception as e:
                self.exception = e
                with open(f'generated/[log] {self.name}.txt', 'a') as f:
                    f.write(str(e))
                    f.write(traceback.format_exc())
            finally:
                self.queue.task_done()


# Customer-Centric Journeys
def play1(df, event: threading.Event = None):
    if not event.is_set():
        return

    core_org_status = df['Core Org Status']
    msg_jour_segm_num = df['Messaging Journeys Segment (Numeric)']
    msg_jour_ord_qty = df['Messaging Journeys Order Quantity']
    pard_ord_qty = df['Pardot Order Quantity']
    mast_carv = df['Master Carve']
    acc_inte_acc_inte_name = df['Account Intelligence: Account Intelligence Name']
    acc_deta_look_acc_deta_name = df['Account Detail Lookup: Account Details Name']

    mclist = ["AMER CAN CMRCL GEN", "AMER CAN CMRCL MID", "AMER CAN ENTR", "AMER CAN SMB GRB", "AMER CAN SMB SB", "AMER CS CMRCL GEN GEO US", "AMER CS CMRCL GEN MFG US", "AMER CS CMRCL MID Geo US", "AMER CS CMRCL MID MFG US", "AMER CS SMB GRB US", "AMER CS SMB LOWTOUCH CA", "AMER CS SMB LOWTOUCH US", "AMER CS SMB SB US", "AMER ENTR CMT", "AMER ENTR HEALTH VERT", "AMER ENTR LARGE ENT", "AMER ENTR LISCI VERT", "AMER ENTR MID ENT", "AMER FINS BANK VERT", "AMER FINS CMRCL GEN CA", "AMER FINS CMRCL GEN US", "AMER FINS CMRCL MID CA", "AMER FINS CMRCL MID US", "AMER FINS INSURANCE VERT", "AMER FINS SMB GRB CA", "AMER FINS SMB GRB US", "AMER FINS SMB SB CA", "AMER FINS SMB SB US", "AMER FS Retail Canada", "AMER HCLS CMRCL GEN CA", "AMER HCLS CMRCL GEN US", "AMER HCLS CMRCL MID CA", "AMER HCLS CMRCL MID US", "AMER HCLS SMB GRB CA", "AMER HCLS SMB GRB US", "AMER HCLS SMB SB CA", "AMER HCLS SMB SB US", "AMER NP EDU FS", "AMER NP EDU MM", "AMER NP EDU MM CAN", "AMER NP EDU MM CC", "AMER NP EDU MM K12", "AMER NP NGO ESB", "AMER NP NGO FS", "AMER NP NGO FS FND", "AMER NP NGO MM", "AMER NP NGO MM CAN", "AMER NP NGO MM LACA", "AMER NP NGO SMB", "AMER PS AD", "AMER PS C", "AMER PS CA", "AMER PS DOD", "AMER PS L", "AMER RCG CMRCL MID US", "AMER RCG CMRCL SMB GRB US", "AMER RCG CMRCL SMB SB US", "AMER RCG ENTR KS CG US", "AMER RCG ENTR KS Retail US", "AMER RCG ENTR Strat CG", "AMER RCG ENTR Strat Retail", "APAC CIT Review", "APAC CS ALL ANZ OCE", "APAC CS EGM GCR TW", "APAC CS ENTR INDIA FINS", "APAC CS ENTR INDIA North", "APAC CS ENTR INDIA South", "APAC CS ENTR INDIA West", "APAC CS ESB ASIA MY", "APAC CS ESB ASIA SG", "APAC CS ESB ASIA TH", "APAC CS ESB ASIA VN", "APAC CS ESB AU", "APAC CS ESB AU FINS", "APAC CS ESB GCR CN SOUTH", "APAC CS ESB GCR HTM", "APAC CS ESB NZ", "APAC CS ESB ROASIA", "APAC CS ESMB ESB INDIA FINS", "APAC CS ESMB ESB INDIA North", "APAC CS ESMB ESB INDIA South", "APAC CS ESMB ESB INDIA West", "APAC CS ESMB SMB INDIA FINS", "APAC CS ESMB SMB INDIA North", "APAC CS ESMB SMB INDIA South", "APAC CS ESMB SMB INDIA West", "APAC CS GB INDIA FINS", "APAC CS GB INDIA North", "APAC CS GB INDIA South", "APAC CS GB INDIA West", "APAC CS GBMM GCR CN EAST", "APAC CS GBMM GCR CN NORTH", "APAC CS GBMM GCR CN SOUTH", "APAC CS GBMM GCR HM", "APAC CS GBSAE INDIA North", "APAC CS GBSAE INDIA South", "APAC CS GBSAE INDIA West", "APAC CS INDIA", "APAC CS MMGB ASIA ID", "APAC CS MMGB ASIA MY", "APAC CS MMGB ASIA PH", "APAC CS MMGB ASIA SG", "APAC CS MMGB ASIA SG FINS", "APAC CS MMGB ASIA SG UNICORN", "APAC CS MMGB ASIA TH", "APAC CS MMGB ASIA VN", "APAC CS MMGB AU", "APAC CS MMGB AU FINS", "APAC CS MMGB NZ", "APAC CS MM INDIA FINS", "APAC CS MM INDIA North", "APAC CS MM INDIA South", "APAC CS MM INDIA West", "APAC CS SMB ASIA MY", "APAC CS SMB ASIA SG", "APAC CS SMB ASIA TH", "APAC CS SMB ASIA VN", "APAC CS SMB AU", "APAC CS SMB AU FINS", "APAC CS SMB GCR CN EAST", "APAC CS SMB GCR CN SOUTH", "APAC CS SMB GCR HTM", "APAC CS SMB NZ", "APAC CS SMB ROASIA", "APAC FS ENTR ASIA ID", "APAC FS ENTR ASIA MY", "APAC FS ENTR ASIA PH", "APAC FS ENTR ASIA SG", "APAC FS ENTR ASIA SG FINS", "APAC FS ENTR ASIA TH", "APAC FS ENTR ASIA VN", "APAC FS ENTR AU", "APAC FS ENTR GCR CN EAST", "APAC FS ENTR GCR CN NORTH", "APAC FS ENTR GCR CN SOUTH", "APAC FS ENTR GCR HM", "APAC FS ENTR NZ", "APAC GCR ESMB HTM NP", "APAC NP ASIA", "APAC NP EDU CBU ANZ", "APAC NP EDU EBU ANZ", "APAC NP EDU K12 ANZ", "APAC NP INDIA ESMB SMB", "APAC NP INDIA MMGB", "APAC NP NGO ESB ANZ", "APAC NP NGO GB ANZ", "APAC NP NGO MM ANZ", "APAC NP NGO SMB ANZ", "APAC PS AU C", "APAC PS AU L", "APAC PS HEALTH", "APAC PS NZ", "APAC PS SG", "BootCamp", "EMEA CS CMRCL GB CEN AT", "EMEA CS CMRCL GB CEN CH", "EMEA CS CMRCL GB CEN DE", "EMEA CS CMRCL GB CEN DE B2C", "EMEA CS CMRCL GB EME AFR", "EMEA CS CMRCL GB EME CEE CIS", "EMEA CS CMRCL GB EME MDE", "EMEA CS CMRCL GB IL", "EMEA CS CMRCL GB NTH BELUX", "EMEA CS CMRCL GB NTH NL", "EMEA CS CMRCL GB NTH NOR", "EMEA CS CMRCL GB STH FR", "EMEA CS CMRCL GB STH IBE", "EMEA CS CMRCL GB STH IT", "EMEA CS CMRCL MM1 CEN DE", "EMEA CS CMRCL MM2 CEN DE", "EMEA CS CMRCL MM CEN AT", "EMEA CS CMRCL MM CEN CH", "EMEA CS CMRCL MM CEN DE B2C", "EMEA CS CMRCL MM EME AFR", "EMEA CS CMRCL MM EME CEE CIS", "EMEA CS CMRCL MM EME MDE", "EMEA CS CMRCL MM IL", "EMEA CS CMRCL MM NTH BELUX", "EMEA CS CMRCL MM NTH NL", "EMEA CS CMRCL MM NTH NOR", "EMEA CS CMRCL MM STH FR", "EMEA CS CMRCL MM STH IBE", "EMEA CS CMRCL MM STH IT", "EMEA CS ESMB EME AFR", "EMEA CS ESMB EME CEE CIS", "EMEA CS ESMB EME MDE", "EMEA CS ESMB ESB CEN CH", "EMEA CS ESMB ESB CEN DE", "EMEA CS ESMB ESB IL", "EMEA CS ESMB ESB NTH BELUX", "EMEA CS ESMB ESB NTH NL", "EMEA CS ESMB ESB NTH NOR", "EMEA CS ESMB ESB STH FR", "EMEA CS ESMB ESB STH IBE", "EMEA CS ESMB ESB STH IT", "EMEA CS ESMB SMB CEN AT", "EMEA CS ESMB SMB CEN CH", "EMEA CS ESMB SMB CEN DE", "EMEA CS ESMB SMB IL", "EMEA CS ESMB SMB NTH BELUX", "EMEA CS ESMB SMB NTH NL", "EMEA CS ESMB SMB NTH NOR", "EMEA CS ESMB SMB SNR CEN AT", "EMEA CS ESMB SMB SNR CEN CH", "EMEA CS ESMB SMB SNR CEN DE", "EMEA CS ESMB SMB SNR IL", "EMEA CS ESMB SMB SNR NTH BELUX", "EMEA CS ESMB SMB SNR NTH NL", "EMEA CS ESMB SMB SNR NTH NOR", "EMEA CS ESMB SMB SNR STH FR", "EMEA CS ESMB SMB SNR STH IBE", "EMEA CS ESMB SMB SNR STH IT", "EMEA CS ESMB SMB STH FR", "EMEA CS ESMB SMB STH IBE", "EMEA CS ESMB SMB STH IT", "EMEA FS ENTR CEN AT", "EMEA FS ENTR CEN CH", "EMEA FS ENTR CEN DE", "EMEA FS ENTR CEN DE B2C", "EMEA FS ENTR EME AFR", "EMEA FS ENTR EME CEE CIS", "EMEA FS ENTR EME MDE", "EMEA FS ENTR IL", "EMEA FS ENTR NTH BELUX", "EMEA FS ENTR NTH NL", "EMEA FS ENTR NTH NOR", "EMEA FS ENTR STH FR", "EMEA FS ENTR STH IBE", "EMEA FS ENTR STH IT", "EMEA NP EDU FS EU", "EMEA NP EDU FS MEA", "EMEA NP NGO ESB EU", "EMEA NP NGO FS EU", "EMEA NP NGO FS MEA", "EMEA NP NGO FS NL", "EMEA NP NGO FS UKI", "EMEA NP NGO MM EU", "EMEA NP NGO MM NL", "EMEA NP NGO MM UKI", "EMEA NP NGO SMB EU", "EMEA PS BNL", "EMEA PS FR", "EMEA PS IE", "EMEA PS UK", "EMEA RU", "Global Junk", "JP CS ESB KR", "JP CS SMB1 KR", "JP CS SMB2 KR", "JP CS TKO GRB", "JP CS TKO SB", "JP CS TKO SGRB", "JP FS ENTR KR", "JP FS GB", "JP FS IND BTC ENTR", "JP FS IND FINS", "JP FS IND MFG ENTR", "JP FS IND PS1", "JP FS TKO MM", "JP NP KR", "JP RS CHU ENTR", "JP RS CHU GB", "JP RS CHU MM", "JP RS E GB", "JP RS E MM", "JP RS GRB", "JP RS KYS GRB", "JP RS OSK ENTR", "JP RS OSK GB", "JP RS OSK GRB", "JP RS OSK MM", "JP RS OSK SB", "JP RS SB", "JP RS W GB", "JP RS W MM", "LACA CS CMCL GB BR", "LACA CS CMCL GB EMG", "LACA CS CMCL GB GRW", "LACA CS CMCL GB MX", "LACA CS CMCL MM BR", "LACA CS CMCL MM EMG", "LACA CS CMCL MM GRW", "LACA CS CMCL MM MX", "LACA CS ESMB ESB BR", "LACA CS ESMB ESB EMG", "LACA CS ESMB ESB GRW", "LACA CS ESMB ESB MX", "LACA CS ESMB SMB BR", "LACA CS ESMB SMB EMG", "LACA CS ESMB SMB GRW", "LACA CS ESMB SMB MX", "LACA FS ENTR BR", "LACA FS ENTR EMG", "LACA FS ENTR GRW", "LACA FS ENTR MX", "Restricted Accounts", "UKI COMM_MEDIA ENTR", "UKI COMM_MEDIA MMGB", "UKI CPG_MFG MMGB", "UKI CPG ENTR", "UKI ESB", "UKI ESB IRELAND", "UKI FINS ENTR", "UKI FINS MMGB", "UKI FINTECH", "UKI HIGHTECH_HLS MMGB", "UKI HIGHTECH ENTR", "UKI HLS ENTR", "UKI IRELAND", "UKI MFG ENTR", "UKI PROF_OTHER ENTR", "UKI PROF_OTHER MMGB", "UKI RETAIL_TTH ENTR", "UKI RETAIL_TTH MMGB", "UKI SCOTLAND", "UKI SMB", "UKI SMB IRELAND", "UKI SMB SNR", "UKI UTILITIES_OIL ENTR", "UKI UTILITIES_OIL MMGB"]
    acvlist = pd.Series([38182.22,28326.65,77059.29,26626.66,16460.68,56999.42,32867.37,37567.54,19864.67,21810.23,15372.52,11395.21,13614.56,170027.11,88152.02,107877.59,95788.37,74661.05,167786.88,33087.10,46751.81,24906.28,33082.80,114578.24,13758.63,21904.16,18539.54,16629.33,106430.27,19564.86,41779.51,26318.54,30310.11,16540.64,22885.32,10898.43,16000.07,38527.51,12607.12,9434.23,13452.74,14395.62,9879.34,51184.96,21631.40,27816.12,5985.71,15494.78,12968.58,27253.84,122575.62,46148.04,160500.36,46205.94,61235.09,31026.18,17319.16,125146.39,108260.52,118787.28,430734.20,18669.29,40880.37,61977.63,22854.75,55605.24,14186.62,32798.49,7216.72,24774.94,21997.80,3099.23,17366.95,16395.55,114946.59,20285.33,13720.06,13722.16,23639.95,19459.35,20735.65,9360.50,28201.91,35169.41,18066.19,51153.19,60884.65,19439.22,9867.60,43927.01,61744.98,87600.00,33723.36,42099.99,71409.66,70576.35,38645.20,14899.20,22248.44,68667.66,406259.27,25000.00,51603.98,53465.80,19795.98,63734.46,35150.74,28660.64,28027.30,137679.65,21655.28,11938.92,10668.38,27258.97,15523.67,5844.00,21489.54,19013.69,18994.60,11322.59,19126.44,25245.02,22095.93,3049.70,68522.68,104478.84,76747.21,82339.80,269478.62,56910.27,74160.45,96887.24,36834.08,36279.84,119967.12,60138.79,51828.49,25148.71,13799.99,47099.04,114277.29,21385.71,5000.04,11931.77,9148.86,19965.46,22199.84,11488.02,69088.62,52227.59,25366.13,55519.22,46219.00,46843.33,30895.13,43756.90,34468.60,40795.62,32955.63,62408.43,20236.29,26800.13,15539.78,41786.61,39991.16,48791.48,33757.98,36665.92,27291.38,32262.23,40823.07,34681.17,21046.47,58378.34,36872.71,28933.85,57759.69,15963.91,29461.73,51110.77,30773.92,31468.08,36643.44,34370.42,19444.32,34341.73,17107.58,15085.57,28650.16,19767.55,10978.67,19319.77,22852.11,14879.13,20568.83,8336.96,45387.80,21564.48,13398.12,7136.35,16763.65,20987.87,33463.39,13395.14,24969.21,20958.40,31606.95,16145.01,15446.67,30861.31,29688.62,28847.87,17265.79,18346.72,23479.61,67536.58,100984.67,53489.17,81428.19,95490.78,56373.38,112872.78,63262.03,91172.65,59346.76,63819.12,82735.37,61825.69,71605.72,26022.31,24267.54,14937.62,43732.72,37124.42,27715.22,39343.17,25086.02,19214.38,20363.99,11394.63,22936.48,94197.84,220696.27,139583.40,76642.28,31639.82,7674.72,19696.90,59500.02,46470.24,33609.47,53015.63,40622.02,66997.85,90268.23,122935.79,70174.31,109914.53,66336.75,20980.14,55435.82,158182.30,60162.95,26117.44,27611.68,53192.48,20909.19,51225.97,330118.10,55623.24,42266.46,20452.23,3000.00,55403.34,70460.18,51742.79,27847.85,33887.32,16946.86,46068.41,27998.31,17114.29,22083.37,88236.96,4802.40,8805.93,11172.46,29908.37,20684.70,12410.23,7204.53,122938.27,48158.17,60860.02,79857.88,110299.22,97657.70,49824.18,28914.00,181840.77,17498.73,10400.82,169496.34,47476.39,40527.87,47674.33,44210.77,96963.60,41835.30,46201.99,65294.94,52449.37,284086.67,63438.19,57545.24,20668.40,47605.01,49748.41,73938.46,72791.75])

    play_cond = ((acc_inte_acc_inte_name != "") & (acc_deta_look_acc_deta_name != "")) & ((core_org_status == "ACTIVE") & (msg_jour_segm_num >= 4) & (msg_jour_ord_qty == 0) & (pard_ord_qty == 0))

    df["FY22-Mktg Customer-Centric Journeys"] = "No"
    df.loc[play_cond, 'FY22-Mktg Customer-Centric Journeys'] = 'Yes'

    df['FY22-Mktg Customer-Centric Journeys ACV'] = 0
    mc_cond = mast_carv.isin(mclist)
    q = df.loc[play_cond]
    mc_q = q.loc[mc_cond]
    acv_rows = acvlist.take(mc_q['Master Carve'].apply(lambda x: mclist.index(x.strip())))
    acv_rows.index = mc_q.index
    df.loc[play_cond, "FY22-Mktg Customer-Centric Journeys ACV"] = 93400
    df.loc[play_cond & mc_cond, "FY22-Mktg Customer-Centric Journeys ACV"] = acv_rows


# Data-Driven Advertising
def play2(df, event: threading.Event = None):
    if not event.is_set():
        return

    core_org_status = df['Core Org Status']
    dato_segm_num = df['Datorama Segment (Numeric)']
    adve_stud_segm_num = df['Advertising Studio Segment (Numeric)']
    dato_ord_qty = df['Datorama Order Qty']
    adve_stud_ord_qty = df['Advertising Studio Order Quantity']
    mast_carv = df['Master Carve']
    acc_inte_acc_inte_name = df['Account Intelligence: Account Intelligence Name']
    acc_deta_look_acc_deta_name = df['Account Detail Lookup: Account Details Name']

    mclist = ["AMER CAN CMRCL MID", "AMER CAN ENTR", "AMER CAN SMB GRB", "AMER CAN SMB SB", "AMER CS CMRCL GEN GEO US", "AMER CS CMRCL GEN MFG US", "AMER CS CMRCL MID Geo US", "AMER CS SMB GRB US", "AMER CS SMB LOWTOUCH CA", "AMER CS SMB LOWTOUCH US", "AMER CS SMB SB US", "AMER ENTR CMT", "AMER ENTR HEALTH VERT", "AMER ENTR LARGE ENT", "AMER ENTR LISCI VERT", "AMER ENTR MID ENT", "AMER FINS BANK VERT", "AMER FINS CMRCL GEN CA", "AMER FINS CMRCL GEN US", "AMER FINS CMRCL MID US", "AMER FINS INSURANCE VERT", "AMER FINS SMB GRB US", "AMER FINS SMB SB US", "AMER FS Retail Canada", "AMER HCLS CMRCL GEN US", "AMER HCLS CMRCL MID US", "AMER HCLS SMB GRB US", "AMER HCLS SMB SB US", "AMER NP NGO FS FND", "AMER NP NGO MM", "AMER PS AD", "AMER PS C", "AMER PS DOD", "AMER PS L", "AMER RCG CMRCL MID US", "AMER RCG CMRCL SMB GRB US", "AMER RCG CMRCL SMB SB US", "AMER RCG ENTR KS CG US", "AMER RCG ENTR KS Retail US", "AMER RCG ENTR Strat CG", "AMER RCG ENTR Strat Retail", "APAC CIT Review", "APAC CS EGM GCR TW", "APAC CS ENTR INDIA North", "APAC CS ENTR INDIA West", "APAC CS ESB AU", "APAC CS ESB GCR HTM", "APAC CS ESB NZ", "APAC CS ESMB ESB INDIA South", "APAC CS ESMB SMB INDIA West", "APAC CS GBMM GCR CN SOUTH", "APAC CS GBMM GCR HM", "APAC CS MMGB ASIA PH", "APAC CS MMGB ASIA SG", "APAC CS MMGB ASIA SG UNICORN", "APAC CS MMGB AU", "APAC CS MMGB NZ", "APAC CS MM INDIA West", "APAC CS SMB ASIA MY", "APAC CS SMB ASIA SG", "APAC CS SMB AU", "APAC CS SMB AU FINS", "APAC CS SMB GCR HTM", "APAC FS ENTR ASIA ID", "APAC FS ENTR ASIA PH", "APAC FS ENTR ASIA SG", "APAC FS ENTR ASIA SG FINS", "APAC FS ENTR AU", "APAC FS ENTR GCR HM", "APAC FS ENTR NZ", "APAC NP EDU EBU ANZ", "APAC NP NGO GB ANZ", "APAC NP NGO MM ANZ", "APAC PS AU C", "APAC PS AU L", "APAC PS NZ", "EMEA CS CMRCL GB CEN CH", "EMEA CS CMRCL GB CEN DE", "EMEA CS CMRCL GB EME MDE", "EMEA CS CMRCL GB IL", "EMEA CS CMRCL GB STH FR", "EMEA CS CMRCL GB STH IBE", "EMEA CS CMRCL MM1 CEN DE", "EMEA CS CMRCL MM2 CEN DE", "EMEA CS CMRCL MM CEN CH", "EMEA CS CMRCL MM IL", "EMEA CS CMRCL MM NTH BELUX", "EMEA CS CMRCL MM NTH NOR", "EMEA CS CMRCL MM STH FR", "EMEA CS CMRCL MM STH IBE", "EMEA CS CMRCL MM STH IT", "EMEA CS ESMB EME AFR", "EMEA CS ESMB EME MDE", "EMEA CS ESMB ESB IL", "EMEA CS ESMB ESB NTH NL", "EMEA CS ESMB ESB NTH NOR", "EMEA CS ESMB ESB STH FR", "EMEA CS ESMB SMB IL", "EMEA CS ESMB SMB NTH NL", "EMEA CS ESMB SMB SNR CEN AT", "EMEA CS ESMB SMB SNR CEN DE", "EMEA CS ESMB SMB SNR IL", "EMEA CS ESMB SMB SNR NTH BELUX", "EMEA CS ESMB SMB SNR NTH NL", "EMEA CS ESMB SMB SNR STH IBE", "EMEA CS ESMB SMB STH IBE", "EMEA FS ENTR CEN CH", "EMEA FS ENTR CEN DE", "EMEA FS ENTR CEN DE B2C", "EMEA FS ENTR EME AFR", "EMEA FS ENTR EME MDE", "EMEA FS ENTR NTH BELUX", "EMEA FS ENTR NTH NL", "EMEA FS ENTR NTH NOR", "EMEA FS ENTR STH FR", "EMEA FS ENTR STH IBE", "EMEA FS ENTR STH IT", "UKI COMM_MEDIA ENTR", "UKI COMM_MEDIA MMGB", "UKI CPG ENTR", "UKI ESB", "UKI FINS ENTR", "UKI HIGHTECH ENTR", "UKI HLS ENTR", "UKI PROF_OTHER ENTR", "UKI PROF_OTHER MMGB", "UKI RETAIL_TTH ENTR", "UKI SMB", "UKI SMB SNR", "UKI UTILITIES_OIL ENTR"]
    acvlist = pd.Series([45013.75,112351.33,27685.99,30103.27,66040.90,27960.00,48102.38,47338.68,38059.74,28346.64,21675.65,112644.60,91336.65,137676.29,118820.50,57147.52,141269.54,54720.00,66883.15,67284.47,9605.57,42127.84,35230.00,121500.00,55038.75,61968.00,35542.33,36450.00,69984.00,22500.01,75553.97,226569.14,63906.14,75000.00,152484.14,37148.29,32176.95,182292.69,82741.10,288260.41,104939.92,127499.99,102000.00,26280.00,31698.86,56511.01,27216.00,17334.48,12150.00,42876.00,22496.67,59683.80,33161.25,34092.00,55854.60,22787.01,23031.75,20160.00,19128.00,45212.48,19967.94,18000.00,35949.72,13923.56,62417.87,37056.96,109608.00,49442.56,58197.33,23310.04,74520.02,24343.21,18977.06,71348.78,57960.01,27192.00,112106.98,42600.25,88998.96,31898.00,39507.60,28594.34,39387.54,41717.18,42160.80,35360.00,37199.98,29004.05,2989.80,36992.55,31482.00,13819.20,13554.79,24741.60,27720.00,57419.99,22070.40,153388.50,35382.59,36432.19,23958.00,25267.80,61749.07,20707.50,20169.60,26525.39,284257.65,89504.94,98108.24,44563.10,52322.88,35861.11,63012.91,87008.81,55545.70,40197.47,70042.34,119128.41,42445.53,226419.59,143107.15,100482.19,154394.81,223219.98,41330.67,14214.13,127893.79,55833.11,52762.08,42385.71])

    play_cond = ((acc_inte_acc_inte_name != "") & (acc_deta_look_acc_deta_name != "")) & ((core_org_status == "ACTIVE") & (dato_segm_num >= 4) & (adve_stud_segm_num >= 4) & (dato_ord_qty == 0) & (adve_stud_ord_qty == 0))

    df["FY22-Mktg Data-Driven Advertising"] = "No"
    df.loc[play_cond, 'FY22-Mktg Data-Driven Advertising'] = 'Yes'

    df['FY22-Mktg Data-Driven Advertising ACV'] = 0
    mc_cond = mast_carv.isin(mclist)
    play_qualified = df.loc[play_cond]
    mc_qualified = play_qualified.loc[mc_cond]
    acvs = acvlist.take(mc_qualified['Master Carve'].apply(lambda x: mclist.index(x.strip())))
    acvs.index = mc_qualified.index
    df.loc[play_cond, "FY22-Mktg Data-Driven Advertising ACV"] = 67800
    df.loc[play_cond & mc_cond, "FY22-Mktg Data-Driven Advertising ACV"] = acvs


# Account-Based Marketing
def play3(df, event: threading.Event = None):
    if not event.is_set():
        return

    core_org_status = df['Core Org Status']
    pard_ptb_segm_num = df['Pardot PTB Segment (Numeric)']
    pard_ord_qty = df['Pardot Order Quantity']
    msg_jour_segm_num = df['Messaging Journeys Segment (Numeric)']
    numb_cont = df['# Contacts']
    acc_inte_acc_inte_name = df['Account Intelligence: Account Intelligence Name']
    acc_deta_look_acc_deta_name = df['Account Detail Lookup: Account Details Name']

    play_cond = ((acc_inte_acc_inte_name != "") & (acc_deta_look_acc_deta_name != "")) & (core_org_status == "ACTIVE") & (pard_ptb_segm_num >= 4) & (pard_ord_qty == 0) & (all([x in [1, 2, 3] for x in msg_jour_segm_num])) & (numb_cont >= 10000)

    df["FY22-Mktg Account-Based Marketing"] = "No"
    df.loc[play_cond, 'FY22-Mktg Account-Based Marketing'] = 'Yes'

    df['FY22-Mktg Account-Based Marketing ACV'] = 0
    contacts = df.loc[play_cond, '# Contacts'].replace(np.nan, 0).astype(int).values
    df.loc[play_cond, "FY22-Mktg Account-Based Marketing ACV"] = (((contacts / 10000) * 1800) + 30000) * 0.75


# Real-Time Personalization
def play4(df, event: threading.Event = None):
    if not event.is_set():
        return

    core_org_status = df['Core Org Status']
    inte_stud_ptb_segm_num = df['Interaction Studio PTB Segment (Num)']
    inte_stud_ord_qty = df['Interaction Studio Order Qty']
    acc_owne_role = df['Account Owner Role']
    mast_carv = df['Master Carve']
    msg_jour_ord_qty = df['Messaging Journeys Order Quantity']
    pard_ord_qty = df['Pardot Order Quantity']

    mclist = ["AMER CAN CMRCL GEN", "AMER CAN CMRCL MID", "AMER CAN ENTR", "AMER CS CMRCL GEN GEO US", "AMER CS CMRCL MID Geo US", "AMER CS SMB GRB US", "AMER ENTR CMT", "AMER ENTR HEALTH VERT", "AMER ENTR LARGE ENT", "AMER ENTR LISCI VERT", "AMER ENTR MID ENT", "AMER FINS BANK VERT", "AMER FINS CMRCL GEN US", "AMER FINS CMRCL MID US", "AMER FINS INSURANCE VERT", "AMER FINS SMB GRB US", "AMER HCLS CMRCL GEN US", "AMER HCLS SMB SB US", "AMER NP EDU MM", "AMER PS C", "AMER PS CA", "AMER RCG CMRCL MID US", "AMER RCG ENTR KS CG US", "AMER RCG ENTR KS Retail US", "AMER RCG ENTR Strat CG", "AMER RCG ENTR Strat Retail", "APAC CS ESB ASIA SG", "APAC CS ESMB SMB INDIA FINS", "APAC FS ENTR ASIA SG", "APAC FS ENTR ASIA TH", "APAC FS ENTR AU", "APAC NP NGO MM ANZ", "APAC PS AU L", "EMEA CS CMRCL GB CEN DE", "EMEA CS CMRCL GB CEN DE B2C", "EMEA CS CMRCL MM EME CEE CIS", "EMEA CS CMRCL MM STH IT", "EMEA CS ESMB EME CEE CIS", "EMEA CS ESMB ESB STH IT", "EMEA FS ENTR CEN AT", "EMEA FS ENTR CEN CH", "EMEA FS ENTR CEN DE", "EMEA FS ENTR CEN DE B2C", "EMEA FS ENTR EME AFR", "EMEA FS ENTR NTH BELUX", "EMEA FS ENTR NTH NL", "EMEA FS ENTR NTH NOR", "EMEA FS ENTR STH FR", "EMEA FS ENTR STH IBE", "EMEA FS ENTR STH IT", "EMEA NP EDU FS EU", "EMEA NP NGO MM UKI", "JP CS TKO GRB", "JP FS GB", "JP FS IND BTC ENTR", "LACA FS ENTR BR", "LACA FS ENTR GRW", "LACA FS ENTR MX", "UKI COMM_MEDIA MMGB", "UKI CPG_MFG MMGB", "UKI CPG ENTR", "UKI FINS ENTR", "UKI FINS MMGB", "UKI FINTECH", "UKI MFG ENTR", "UKI PROF_OTHER MMGB", "UKI RETAIL_TTH ENTR", "UKI SMB", "UKI UTILITIES_OIL ENTR"]
    acvlist = pd.Series([109492.01,229206.78,113295.49,399236.80,5850.00,77195.00,465888.83,223029.15,285437.36,186693.57,260059.55,97429.39,137544.04,125000.00,97304.18,13878.28,2235.00,77700.00,9813.00,161214.12,96019.86,300000.00,140455.79,114402.40,68755.27,134027.43,90500.00,73960.25,100368.00,105705.72,226412.30,33120.01,126133.41,76781.79,8771.52,246259.18,100899.07,123120.00,237863.98,154442.67,131328.35,258902.77,240348.46,292806.15,259626.55,59252.13,213713.00,163111.59,98676.08,110388.61,119533.91,197146.81,67157.52,115697.70,432754.17,287454.85,100790.81,43723.58,114752.11,99999.95,91825.61,159892.38,111630.48,192565.06,162964.01,362742.18,295164.70,207224.73,69742.27])

    play_cond = (core_org_status == "ACTIVE") & (inte_stud_ptb_segm_num >= 3) & (inte_stud_ord_qty == 0) & ((msg_jour_ord_qty >= 1) | (pard_ord_qty >= 1)) & (~acc_owne_role.isin(["SMB", "ESSENTIALS"]))

    df["FY22-Mktg Real-Time Personalization"] = "No"
    df.loc[play_cond, 'FY22-Mktg Real-Time Personalization'] = 'Yes'

    df['FY22-Mktg Real-Time Personalization ACV'] = 0
    mc_cond = mast_carv.isin(mclist)
    play_qualified = df.loc[play_cond]
    mc_qualified = play_qualified.loc[mc_cond]
    acvs = acvlist.take(mc_qualified['Master Carve'].apply(lambda x: mclist.index(x.strip())))
    acvs.index = mc_qualified.index
    df.loc[play_cond, "FY22-Mktg Real-Time Personalization ACV"] = 168000
    df.loc[play_cond & mc_cond, "FY22-Mktg Real-Time Personalization ACV"] = acvs


# Unify Your Data (CDP)
def play5(df, event: threading.Event = None):
    if not event.is_set():
        return

    msg_jour_ord_qty = df['Messaging Journeys Order Quantity']
    regi = df['Region.']
    pard_ord_qty = df['Pardot Order Quantity']
    core_org_status = df['Core Org Status']
    numb_cont = df['# Contacts']
    msg_jour_segm_num = df['Messaging Journeys Segment (Numeric)']

    play_cond = (msg_jour_ord_qty == 0) & (regi == "AMER") & (pard_ord_qty == 0) & (core_org_status == "ACTIVE") & (numb_cont >= 100000) & (msg_jour_segm_num >= 4)

    df["FY22-Mktg Unify Your Data (CDP)"] = "No"
    df.loc[play_cond, 'FY22-Mktg Unify Your Data (CDP)'] = 'Yes'

    df['FY22-Mktg Unify Your Data (CDP) ACV'] = 0
    df.loc[play_cond, "FY22-Mktg Unify Your Data (CDP) ACV"] = 150000