import json as js
import shutil
import sys
import threading
import time
from os import system, makedirs, path
from pathlib import Path

from oracle_to_csv_multithread import execute_from_sql_file
from utils import print_at, progress_bar_with_time, seconds_to_clock


def my_function(th_id, filepath, filedest):
    with progress_bar_with_time(prefix=f"Executing {filepath}", line=th_id, clear=False):
        shutil.move(execute_from_sql_file(filepath=filepath), filedest)


class MyThreadClass(threading.Thread):
    id = -1

    def __init__(self, filepath, filedest, reset_id=False):
        threading.Thread.__init__(self)
        self.filepath = filepath
        self.filedest = filedest
        self.expand_home()

        self.reset_id() if reset_id else None
        self.__class__.id += 1

    @classmethod
    def reset_id(cls):
        cls.id = -1

    def expand_home(self):
        home = str(Path.home())
        self.filedest = self.filedest.replace('~', home)
        self.filepath = self.filepath.replace('~', home)

    def run(self):
        my_function(self.__class__.id, self.filepath, self.filedest)


def query_azprd(azprd_queries, account_intelligence=True):
    system('clear')
    MyThreadClass.reset_id()

    if '--no-query' not in sys.argv:
        time_o = time.time()
        threads = []

        try:
            for query in azprd_queries:
                if query['status'] == "active":
                    th = MyThreadClass(query['sql_file'], query['filepath'])
                    threads.append(th)

                    # Creates output directory if it doesn't exist
                    sep = path.sep
                    output_directory = sep.join(th.filedest.split(sep)[:-1])
                    if not path.exists(output_directory):
                        makedirs(output_directory)

                    # Start thread
                    th.start()
        except KeyboardInterrupt as e:
            print_at(e, len(azprd_queries) + 1)
        finally:
            for th in threads:
                th.join()

        print(f" Process time: {seconds_to_clock(time.time() - time_o)}")

    if account_intelligence:
        print("\n === EXECUTING build_ai_dataset.py ===\n")
        system('cd ~/Git/prospecting-placemats && python3 build_ai_dataset.py')

    return len(azprd_queries) if '--no-query' not in sys.argv else 0  # only for printing purpose


if __name__ == '__main__':
    json = js.load(open('input/azprd_queries.json'))
    query_azprd(json)
