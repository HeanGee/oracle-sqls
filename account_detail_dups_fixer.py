import csv
import json as js
import os
import re

import pandas as pd


def fix_account_details(js_data):
    digital360_filenames = []
    folder = str(os.path.join(os.path.dirname(__file__), js_data['folder']))
    for filename in os.listdir(folder):
        if re.match(rf'{js_data["filename_pattern"]}', filename) is not None:
            digital360_filenames.append(filename)
    digital360 = max(digital360_filenames)  # Get the latest Digital360 file

    detail_csv = csv.DictReader(open(f"{folder}{js_data['detail']}"))
    dset_tofix = pd.read_csv(f"{folder}{digital360}")
    reference_csv = pd.read_csv(f"{folder}{js_data['old-dataset']}")
    reference_csv = reference_csv.rename(columns={'Total # Of Licenses': '# Total Licenses'})

    duption_scoreboard = dset_tofix.duplicated(subset=['Account ID'])
    dupped_rowindex_list = []  # row index at which occurs the dup
    for i, j in duption_scoreboard.items():
        dupped_rowindex_list.append(i) if j else None

    # Lists all accounts with duplicated details
    dupped_ids = set(dset_tofix.iloc[dupped_rowindex_list]['Account ID'].values)
    # List all accounts only in dupped but not in old
    dupped_notin_old = dupped_ids - set(reference_csv['Account ID'].values)

    # Flattens duplicates grouping by 'Account ID' column
    dupped_accts = dset_tofix.loc[dset_tofix['Account ID'].isin(dupped_ids)].groupby(['Account ID'],
                                                                                     as_index=False).last()

    # Selects only columns not in detail
    # Source: https://stackoverflow.com/questions/40636514/selecting-pandas-dataframe-column-by-list
    detail_columns = (set(detail_csv.fieldnames) - {'Created Date', 'Last Modified Date'})
    columns_without_details = set(dset_tofix.columns.values) - (detail_columns - {'Account ID'})
    dupped_accts = dupped_accts[dupped_accts.columns.intersection(columns_without_details)]

    # Removes dupped accounts not in old
    dupped_accts = dupped_accts[~dupped_accts['Account ID'].isin(dupped_notin_old)].reset_index(drop=True)

    # Gets A.D. column values from old dataset
    ad_from_old = reference_csv.loc[reference_csv['Account ID'].isin(dupped_ids)][detail_columns].reset_index(drop=True)

    # Restore detail's columns with the correct value
    # coming from the previous intelligence dataset.
    fixed_accts = pd.merge(ad_from_old, dupped_accts, left_on='Account ID', right_on='Account ID', how='left',
                           indicator=False)

    # Restore the order of columns
    fixed_accts = fixed_accts[dset_tofix.columns]

    # Removes all duplicated accounts from the dset_tofix dataset
    dset_tofix = dset_tofix[~dset_tofix['Account ID'].isin(dupped_ids)]

    # Put back fixed rows
    dset_tofix = pd.concat([dset_tofix, fixed_accts]).reset_index(drop=True)

    # Write back to the file
    return dset_tofix, digital360


def fix_detail(to_fix: pd.DataFrame = None, fix_with: pd.DataFrame = None):
    details = to_fix
    reference_csv = fix_with
    reference_csv = reference_csv.rename(columns={'Total # Of Licenses': '# Total Licenses'})

    duption_scoreboard = details.duplicated(subset=['Account ID'])
    dupped_rowindex_list = []  # row index at which occurs the dup
    for i, j in duption_scoreboard.items():
        dupped_rowindex_list.append(i) if j else None

    # Lists all accounts with duplicated details
    dupped_ids = set(details.iloc[dupped_rowindex_list]['Account ID'].values)
    # List all accounts only in dupped but not in old
    dupped_notin_reference = dupped_ids - set(reference_csv['Account ID'].values)

    # Flattens duplicates grouping by 'Account ID' column
    duplicated_accounts = details.loc[details['Account ID'].isin(dupped_ids)].groupby(['Account ID'],
                                                                                      as_index=False).last()

    # Selects only columns not in detail
    # Source: https://stackoverflow.com/questions/40636514/selecting-pandas-dataframe-column-by-list
    detail_columns = (set(reference_csv.columns) - {'Created Date', 'Last Modified Date'})
    no_detail_columns = set(details.columns.values) - (detail_columns - {'Account ID'})
    acct_without_detail_column = duplicated_accounts[duplicated_accounts.columns.intersection(no_detail_columns)]

    # Removes dupped accounts not in old
    acct_without_detail_column = acct_without_detail_column[
        ~acct_without_detail_column['Account ID'].isin(dupped_notin_reference)].reset_index(drop=True)

    # Gets A.D. column values from old dataset
    data_from_reference = reference_csv.loc[reference_csv['Account ID'].isin(dupped_ids)][detail_columns].reset_index(
        drop=True)

    # Restore detail's columns with the correct value
    # coming from the previous intelligence dataset.
    fixed_accts = pd.merge(data_from_reference, acct_without_detail_column, left_on='Account ID', right_on='Account ID',
                           how='left', indicator=False)

    # Restore the order of columns
    fixed_accts = fixed_accts[details.columns]

    # Removes all duplicated accounts from the details dataset
    details = details[~details['Account ID'].isin(dupped_ids)]

    # Put back fixed rows
    details = pd.concat([details, fixed_accts]).reset_index(drop=True)

    # Write back to the file
    return details


if __name__ == "__main__":
    json = js.load(open('input/account_detail_fixer_inputs.json'))
    fixed, newname = fix_account_details(json)
    outdir = str(os.path.join(os.path.dirname(__file__), json['folder']))
    fixed.to_csv(f"{outdir}[fixed] {newname}")
