import csv
import json as js
import os
import sys
import threading as thm
from datetime import datetime

import numpy as np
import pandas as pd
import tzlocal

from PlacematFY22_Commerce import FY22_Commerce
from PlacematFY22_Marketing import FY22_Marketing
from account_detail_dups_fixer import fix_detail
from hls_extractor_multithreading import query_azprd
from utils import get_numeric_field, progress_bar_with_time


def current_datetime(add_time=False):
    utc = tzlocal.get_localzone()
    return datetime.now(tz=utc).strftime(f"%Y-%m-%d{' %H.%M.%S%z' if add_time else ''}")


def calculate_segment(df, locked_employees):
    df['my-segment'] = 'SMB'
    df.loc[locked_employees >= 200, 'my-segment'] = 'CMRCL'
    df.loc[locked_employees >= 4500, 'my-segment'] = 'ENTR'


def run_builder(builder_metadata: js = None):
    # Gets required paths for reports-joined file and final csv file.
    extended = builder_metadata['org62-reports-joined-path'].replace('<x>', current_datetime())
    extended = os.path.join(os.path.dirname(__file__), extended)
    final_csv_path = builder_metadata['output-csv-path'].replace('<x>', current_datetime(add_time=False))

    # Executes query against AZPRD
    line = query_azprd(builder_metadata['azprd-sql-files'], account_intelligence=False)

    # If the reports-joined csv doesn't exists
    if not os.path.isfile(extended) or '--new' in sys.argv:
        accounts = None
        intelligences = None
        acc_w_di_nm = None

        # Loads Accounts
        with progress_bar_with_time(prefix="Loading Accounts", line=line, clear=False):
            for nth in range(builder_metadata['accounts']['amount']):
                filename = builder_metadata['accounts']['filename'].replace('<x>', str(nth + 1))
                if accounts is None:
                    accounts = pd.read_csv(filename, dtype='unicode')
                    columns = accounts.columns.tolist()
                else:
                    accounts = pd.concat([accounts, pd.read_csv(filename, dtype='unicode')[columns]])
            accounts = accounts.drop_duplicates().reset_index(drop=True)

        # Fixes removing unneeded backslash character in the description field for the account "001000000000JBz"
        line += 1
        with progress_bar_with_time(prefix="Fixing Account description field", line=line, clear=False):
            fixed_value = accounts.loc[accounts["Account ID"] == "001000000000JBz", "Description"].values[0].replace(
                '\\', '')
            accounts.loc[accounts['Account ID'] == "001000000000JBz", 'Description'] = fixed_value

        # Loads Intelligences
        line += 1
        with progress_bar_with_time(prefix="Loading Intelligences", line=line, clear=False):
            for nth in range(builder_metadata['intelligences']['amount']):
                filename = builder_metadata['intelligences']['filename'].replace('<x>', str(nth + 1))
                if intelligences is None:
                    intelligences = pd.read_csv(filename, dtype='unicode')
                    columns = intelligences.columns.tolist()
                else:
                    intelligences = pd.concat([intelligences, pd.read_csv(filename, dtype='unicode')[columns]])
            intelligences = intelligences.drop_duplicates().reset_index(drop=True)

        # Loads Details
        line += 1
        with progress_bar_with_time(prefix="Loading Details", line=line, clear=False):
            details = pd.read_csv(builder_metadata['details']['filename'], dtype='unicode')

        # Loads Accounts with Detail/Intelligence Names
        line += 1
        with progress_bar_with_time(prefix="Loading Account w Det. Int. Names.csv", line=line, clear=False):
            acc_w_di_nm = pd.read_csv(builder_metadata['accounts-with-det-int-names'], dtype='unicode')

        # Fixes Multi Details issue
        line += 1
        with progress_bar_with_time(prefix="Fixing multi detail issue", line=line, clear=False):
            detail_reference = pd.read_csv(builder_metadata['detail-fixer']['file'], dtype='unicode')
            details_fixed = fix_detail(to_fix=details, fix_with=detail_reference)

        # Left-join with Details
        line += 1
        with progress_bar_with_time(prefix="Left-joining with Fixed Details", line=line, clear=False):
            dataset = pd.merge(accounts, details_fixed, left_on=builder_metadata['details']['left_on'],
                               right_on=builder_metadata['details']['right_on'],
                               how=builder_metadata['details']['join_method'],
                               suffixes=tuple(builder_metadata['details']['suffixes']),
                               indicator=builder_metadata['details']['indicator'])

        # Left-join with Intelligences
        line += 1
        with progress_bar_with_time(prefix="Left-joining with Intelligences", line=line, clear=False):
            dataset = pd.merge(dataset, intelligences, left_on=builder_metadata['intelligences']['left_on'],
                               right_on=builder_metadata['intelligences']['right_on'],
                               how=builder_metadata['intelligences']['join_method'],
                               suffixes=tuple(builder_metadata['details']['suffixes']),
                               indicator=builder_metadata['intelligences']['indicator'])

        # Left-join with Det/Int Names
        line += 1
        with progress_bar_with_time(prefix="Left-joining Account Detail/Intelligence Names", line=line, clear=False):
            dataset = pd.merge(dataset, acc_w_di_nm, left_on='Account ID', right_on='Account ID', how='left',
                               suffixes=("", "_y"))

        # Extends with AZPRD columns
        if '--no-azprd' not in sys.argv:
            line += 1
            with progress_bar_with_time(prefix="Extending with azprd columns", line=line, clear=False):
                azprd_metadata = builder_metadata['azprd-metadata']
                for metadata in azprd_metadata:
                    if metadata['status'] != "active":
                        continue

                    if metadata['columns']:
                        file = pd.read_csv(metadata['filename'], usecols=metadata['columns'], dtype='unicode')
                    else:
                        file = pd.read_csv(metadata['filename'], dtype='unicode')

                    file = file.rename(columns=metadata['rename-columns'])
                    dataset = pd.merge(dataset, file, left_on='Account ID', right_on='Account ID', how='left',
                                       indicator=False)

        # Replaces Numpy NaN value to zero
        line += 1
        with progress_bar_with_time(prefix="Replacing Numpy NaN values to zeros", line=line, clear=False):
            columns = get_numeric_field(dataset)
            dataset[columns] = dataset[columns].replace(np.nan, 0).astype(float)

        # Removes '_y' columns
        line += 1
        with progress_bar_with_time(prefix='Removing "_y" columns', line=line, clear=False):
            columns = [col for col in dataset.columns if "_y" in col]
            dataset = dataset.drop(columns, axis=1)

        # Saves the Dataset to file
        line += 1
        with progress_bar_with_time(prefix="Saving dataset", line=line, clear=False):
            dataset.to_csv(extended, index=False)
    # If the reports-joined csv doesn't exists
    else:
        # Load dataset
        line += 0 if not line else 3
        with progress_bar_with_time(prefix=f"Loading {extended.split(sep='/')[-1:][0]}", line=line, clear=False):
            columns = csv.DictReader(open(extended)).fieldnames
            columns = set(columns)
            columns -= set(FY22_Marketing.PLACEMAT_FIELDS)
            columns -= set(FY22_Commerce.PLACEMAT_FIELDS)
            columns = ['Account ID', 'Locked Employees'] + FY22_Marketing.OPERATION_FIELDS
            # columns = ['Account ID', 'Locked Employees'] + FY22_Commerce.OPERATION_FIELDS
            columns = set(columns) - {'my-segment'}

            dataset = pd.read_csv(extended, dtype='unicode', usecols=columns, nrows=None)

        # Replaces Numpy NaN value to zero
        line += 1
        with progress_bar_with_time(prefix="Replacing Numpy NaN values to zeros", line=line, clear=False):
            columns = get_numeric_field(dataset)
            dataset[columns] = dataset[columns].replace(np.nan, 0).astype(float)

    line += 1
    with progress_bar_with_time(prefix="Calculating custom Segment", line=line, clear=False):
        calculate_segment(dataset, dataset['Locked Employees'].replace(np.nan, 0).astype(int).values)

    line += 2
    # Creates thread for each placemat
    threads = []
    main_th_event = thm.Event()
    main_th_event.set()

    # Generating plays for Commerce
    commerce_th = FY22_Commerce(df=dataset, line=line, event=main_th_event)

    # Generating plays for Marketing
    marketing_th = FY22_Marketing(df=dataset, line=line, event=main_th_event)

    # Stacks the placemat threads
    # threads.append(commerce_th)
    threads.append(marketing_th)

    line += 1
    with progress_bar_with_time('Generating plays fields', line=line, clear=False, last=True):
        print()  # print(f'\n' * line + "========Now Threads============\n\n")
        try:
            # Runs the threads
            for th in threads:
                th.start()

            # Wait for the threads to finish
            for th in threads:
                th.join()
        except KeyboardInterrupt:
            main_th_event.clear()

            for th in threads:
                th.join()

                for w in th.workers:
                    if w.exception:
                        print(w.exception)

    # Saves the Dataset to file
    # if main_th_event.is_set():
    #     line += 1
    #     with progress_bar_with_time(prefix="Saving dataset", line=line, clear=False, last=True):
            dataset.to_csv(final_csv_path, index=False)

    print('\n\nFINISHED\n')


if __name__ == "__main__":
    metadata_file = open('input/azprd-to-digital360.json')
    metadata = js.load(metadata_file)

    run_builder(metadata)

    metadata_file.close()
