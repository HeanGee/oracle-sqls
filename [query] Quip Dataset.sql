--select ptb_product, ptb_product_clean, product_clean_name, ptb_product_display_name from bds_ops.UF_FMO_PROD_PAI_DETAIL
--group by ptb_product, ptb_product_clean, product_clean_name, ptb_product_display_name
--order by ptb_product
--
--select product, ptb_product, ptb_product_clean, ptb_product_display_name from bds_ops.uf_fmo_details
--group by product, ptb_product, ptb_product_clean, ptb_product_display_name
--order by product;


WITH
    PlacematAccounts   AS (
        SELECT SUBSTR(AccountDetails.accountid, 0, 15) accountid
        FROM ODS.ODS_ACCOUNT_DETAIL__C AccountDetails
        WHERE AccountDetails.DEL_FLG = 'N'
        union
        SELECT AccountInt.ACCOUNT__C accountid
        FROM ODS.ODS_ACCOUNT_INTELLIGENCE__C AccountInt
        WHERE AccountInt.DEL_FLG = 'N'
    ), -- 4.351.599  Rows

    PTBOrderQuantities as (
        select
            account.acct_id accountid,
            min(account.ptb_product_display_name) product,
            sum(ord_qty) order_quantity,
            sum(ptb_segment) ptb_segment,
            sum(product_acv_potential) product_acv_potential
        from
            bds_ops.uf_fmo_details account
        where
            ord_qty is not null
            and ord_qty > 0
        group by
            account.acct_id, account.product  -- PER ACCOUNT && PER PRODUCT
    ), -- 578.774  Rows

    AccountIntelligence as (
        select
            account__c accountid,
            ADVERTISING_STUDIO_ORD_QTY__C advertising_studio_ord_qty,
            B2B_COMMERCE_ORDER_QTY__C b2b_commerce_ord_qty,
            CLOUDCRAZE_SEG_NUMERIC__C B2B_Commerce_PTB_Segment,
            COMMERCE_CLOUD_SEG_NUMERIC__C B2C_COMMERCE_PTB_SEGMENT,
            COMMERCE_CLOUD_ORDER_QTY__C B2C_COMMERCE_ORDER_QTY,
            SALES_CLOUD_ORDER_QTY__C CORE_SALES_ORDER_QTY,
            SERVICE_CLOUD_ORDER_QTY__C CORE_SERVICE_ORDER_QTY,
            SALES_CLOUD_OPEN_PIPE_AMT__C CORE_SALES_OPEN_PIPE_AMOUNT,
            SERVICE_CLOUD_OPEN_PIPE_AMT__C CORE_SERVICE_OPEN_PIPE_AMOUNT,
            CUST_COMMUNITIES_ORDER_QTY__C CUSTOMER_COMMUNITIES_ORDER_QTY,
            FIN_SERVICES_PTB_SCORE__C Financial_serv_ptb_score,
            FIN_SVC_PTB_SEG_NUMERIC__C FINANCIAL_SRV_PTB_SEGMENT,
            APP_CLOUD_ORDER_QTY__C Lightning_Platform_Order_Qty,
            MANUFACTURING_CLD_PTB_SCORE__C MANUFACTURING_CLOUD_PTB_SCORE,
            MANFCT_CLOUD_PTB_SEG_NUM__C MANUFACT_CLOUD_PTB_SEGMENT,
            MESSAGING_JOURNEYS_ORD_QTY__C messaging_journeys_order_qty,
            PRTNR_COMMUNITIES_ORD_QTY__C PARTNER_COMMUNITIES_ORDER_QTY,
            QUIP_PTB_SCORE__C QUIP_PTB_SCORE,
            QUIP_PTB_SEGMENT_NUMERIC__C QUIP_PTB_SEGMENT,
            SOCIAL_STUDIO_ORDER_QTY__C SOCIAL_STUDIO_ORDER_QTY
        from
            ods.ods_account_intelligence__c
        where
            del_flg = 'N'
            and isdeleted = 'false'
            and RECORDTYPEID is not null -- Remove timing issue for deletion marks
    ),

    Account as (
        select
            account.id_15 accountid,
            account.REGION__C account_owner_region,
            account.COMPANY__C company,
            account.GLOBAL_COMPANY__C global_company,
            account.COMBO_SECTOR__C combo_sector,
            account.LOCKED_EMPLOYEES__C locked_employees,
            account.INDUSTRY industry,
            account.segment__c segment
        from
            ODS.ODS_ACCOUNT account
    ),

    AccountDetail as (
        select
            substr(account.accountid, 0, 15) accountid,
            account.of_licenses_crm__c number_of_licenses_crm,
            account.lightning_mau__c lightning_mau
        from
            ODS.ODS_ACCOUNT_DETAIL__C account
    ),

    -- ONLY FOR QUIP PLACEMAT
    AETable AS (
        SELECT
            Teamtable.SFBASE__ACCOUNT__C accountid,
            TableAE.Name AE_Name,
            TableAEM.name AE_Manager
            --ManagerOfAEManager.name AE_Manager_Manager,
            --TeamRole.Name AE_Role
        FROM
            ODS.ODS_SFBASE__SALESFORCETEAM__C Teamtable

            -- Getting Account Executives.
            LEFT JOIN ODS.ODS_USER TableAE
            ON TableAE.ID_15 = Teamtable.sfbase__user__c

            -- Getting Managers of AEs.
            LEFT JOIN ODS.ODS_USER TableAEM
            ON TableAEM.ID = TableAE.managerid

            -- Getting Managers of AEs Managers.
            --LEFT JOIN ODS.ODS_USER ManagerOfAEManager
            --ON ManagerOfAEManager.ID = TableAEM.managerid

            -- Getting Roles.
            LEFT JOIN ODS.ODS_TEAM_ROLE__C TeamRole
            ON TeamRole.ID_15 = Teamtable.TEAM_ROLE__C

        WHERE
            (
                TeamRole.Name like '%ECS%' -- THERE IS 'ECS' AND 'ECS MC'
            )
            AND Teamtable.SFBASE__ENDDATE__C is null
            AND Teamtable.TERRITORY__C is not null
            AND coalesce(Teamtable.ISDELETED, 'true' ) <> 'true'
            AND Teamtable.DEL_FLG = 'N'
    ),

    MgmtChain as (
        select
            Usser.ID userid,
            Usser.ID_15 userid_15,
            Lvl01.name mgmt_chain_level_01,
            Lvl02.name mgmt_chain_level_02,
            Lvl03.name mgmt_chain_level_03,
            Lvl04.name mgmt_chain_level_04,
            Lvl05.name mgmt_chain_level_05,
            Lvl06.name mgmt_chain_level_06,
            Lvl07.name mgmt_chain_level_07,
            Lvl08.name mgmt_chain_level_08,
            Lvl09.name mgmt_chain_level_09,
            Lvl10.name mgmt_chain_level_10,
            Lvl11.name mgmt_chain_level_11,
            Lvl12.name mgmt_chain_level_12
        from
            ods.ods_user Usser

            left join ods.ods_user Lvl01
            on Usser.mgmt_chain_level_01__c = Lvl01.id

            left join ods.ods_user Lvl02
            on Usser.mgmt_chain_level_02__c = Lvl02.id

            left join ods.ods_user Lvl03
            on Usser.mgmt_chain_level_03__c = Lvl03.id

            left join ods.ods_user Lvl04
            on Usser.mgmt_chain_level_04__c = Lvl04.id

            left join ods.ods_user Lvl05
            on Usser.mgmt_chain_level_05__c = Lvl05.id

            left join ods.ods_user Lvl06
            on Usser.mgmt_chain_level_06__c = Lvl06.id

            left join ods.ods_user Lvl07
            on Usser.mgmt_chain_level_07__c = Lvl07.id

            left join ods.ods_user Lvl08
            on Usser.mgmt_chain_level_08__c = Lvl08.id

            left join ods.ods_user Lvl09
            on Usser.mgmt_chain_level_09__c = Lvl09.id

            left join ods.ods_user Lvl10
            on Usser.mgmt_chain_level_10__c = Lvl10.id

            left join ods.ods_user Lvl11
            on Usser.mgmt_chain_level_11__c = Lvl11.id

            left join ods.ods_user Lvl12
            on Usser.mgmt_chain_level_12__c = Lvl12.id

        --where Lvl01.name is not null
    )

select
    -- Accounts Infos
    acct_id accountid,
    min(acct_nm) account_name, min(acct_own_name) account_owner_name, min(acct_own_role_nm) account_owner_role,
    min(Account.account_owner_region) account_owner_region, min(Account.locked_employees) locked_employees,
    min(acct_acv_potential) account_acv_potential, min(acct_typ) account_type,

    -- Accounts management chain levels (12)
    min(MgmtChain.mgmt_chain_level_01) mgmt_chain_level_01, min(MgmtChain.mgmt_chain_level_02) mgmt_chain_level_02,
    min(MgmtChain.mgmt_chain_level_03) mgmt_chain_level_03, min(MgmtChain.mgmt_chain_level_04) mgmt_chain_level_04,
    min(MgmtChain.mgmt_chain_level_05) mgmt_chain_level_05, min(MgmtChain.mgmt_chain_level_06) mgmt_chain_level_06,
    min(MgmtChain.mgmt_chain_level_07) mgmt_chain_level_07, min(MgmtChain.mgmt_chain_level_08) mgmt_chain_level_08,
    min(MgmtChain.mgmt_chain_level_09) mgmt_chain_level_09, min(MgmtChain.mgmt_chain_level_10) mgmt_chain_level_10,
    min(MgmtChain.mgmt_chain_level_11) mgmt_chain_level_11, min(MgmtChain.mgmt_chain_level_12) mgmt_chain_level_12,

    -- Companies Infos
    min(company_id) company_id, min(company_nm) company_name,
    min(combo_company_id) combo_company_id, min(combo_company_nm) combo_company_name,
    min(company_global_id) global_company_id, min(global_company_name) global_company_name,
    min(Account.company) company, min(Account.global_company) global_company,

    -- Industries, Sectors, Regions, Segments.
    min(Account.industry) industry, min(derived_industry) derived_industry,
    min(derived_sub_sector) derived_subsector,
    min(locked_sector) locked_sector, min(Account.combo_sector) combo_sector,
    min(segment) segment,

    -- ECS AEs Manager chain
    min(ECS_TABLE.AE_Name) QUIP_ECS_AE,
    min(ECS_TABLE.AE_Manager) QUIP_ECS_MANAGER,

    -- Extras
    min(aov_band) aov_band, coalesce(min(AccountDetail.number_of_licenses_crm), 0) number_of_licenses_crm,
    coalesce(min(AccountDetail.lightning_mau), 0) lightning_mau,

    -- Order Quantities and PTB Infos from BDS_OPS.UF_FMO_DETAILS
    coalesce(min(SalesCloud.order_quantity), 0) sales_core_order_qty,
    coalesce(min(SalesCloud.ptb_segment), 0) sales_core_ptb_segment,
    coalesce(min(ServiceCore.order_quantity), 0) service_core_order_qty,
    coalesce(min(ServiceCore.ptb_segment), 0) service_core_ptb_segment,
    coalesce(min(FinancialService.order_quantity), 0) financial_service_order_qty,
    coalesce(min(FinancialService.ptb_segment), 0) financial_service_ptb_segment,
    coalesce(min(HealthCloud.order_quantity), 0) health_cloud_order_qty,
    coalesce(min(HealthCloud.ptb_segment), 0) health_cloud_ptb_segment,
    coalesce(min(LightninghtPlatform.order_quantity), 0) lighting_platofmr_order_qty,
    coalesce(min(LightninghtPlatform.ptb_segment), 0) lighting_platform_ptb_segment,
    coalesce(min(ManufacturingCloud.order_quantity), 0) manufacturing_cloud_order_qty,
    coalesce(min(ManufacturingCloud.ptb_segment), 0) manufact_cloud_ptb_segment,
    coalesce(min(Quip.order_quantity), 0) quip_order_qty, coalesce(min(Quip.ptb_segment), 0) quip_ptb_segment,

    -- Order Quantities and PTB Infos from ODS.ODS_ACCOUNT_INTELLIGENCE__C
    coalesce(min(AccountIntelligence.advertising_studio_ord_qty), 0) advertising_studio_ord_qty,
    coalesce(min(AccountIntelligence.b2b_commerce_ord_qty), 0) b2b_commerce_ord_qty,
    coalesce(min(AccountIntelligence.B2B_Commerce_PTB_Segment), 0) B2B_Commerce_PTB_Segment,
    coalesce(min(AccountIntelligence.B2C_COMMERCE_PTB_SEGMENT), 0) B2C_COMMERCE_PTB_SEGMENT,
    coalesce(min(AccountIntelligence.B2C_COMMERCE_ORDER_QTY), 0) B2C_COMMERCE_ORDER_QTY,
    coalesce(min(AccountIntelligence.CORE_SALES_ORDER_QTY), 0) CORE_SALES_ORDER_QTY,
    coalesce(min(AccountIntelligence.CORE_SERVICE_ORDER_QTY), 0) CORE_SERVICE_ORDER_QTY,
    coalesce(min(AccountIntelligence.CORE_SALES_OPEN_PIPE_AMOUNT), 0) CORE_SALES_OPEN_PIPE_AMOUNT,
    coalesce(min(AccountIntelligence.CORE_SERVICE_OPEN_PIPE_AMOUNT), 0) CORE_SERVICE_OPEN_PIPE_AMOUNT,
    coalesce(min(AccountIntelligence.CUSTOMER_COMMUNITIES_ORDER_QTY), 0) CUSTOMER_COMMUNITIES_ORDER_QTY,
    coalesce(min(AccountIntelligence.Financial_serv_ptb_score), 0) Financial_service_ptb_score,
    coalesce(min(AccountIntelligence.FINANCIAL_SRV_PTB_SEGMENT), 0) FINANCIAL_SERVICE_PTB_SEGMENT,
    coalesce(min(AccountIntelligence.Lightning_Platform_Order_Qty), 0) Lightning_Platform_Order_Qty,
    coalesce(min(AccountIntelligence.MANUFACTURING_CLOUD_PTB_SCORE), 0) MANUFACTURING_CLOUD_PTB_SCORE,
    coalesce(min(AccountIntelligence.MANUFACT_CLOUD_PTB_SEGMENT), 0) MANUFACT_CLOUD_PTB_SEGMENT,
    coalesce(min(AccountIntelligence.messaging_journeys_order_qty), 0) messaging_journeys_order_qty,
    coalesce(min(AccountIntelligence.PARTNER_COMMUNITIES_ORDER_QTY), 0) PARTNER_COMMUNITIES_ORDER_QTY,
    coalesce(min(AccountIntelligence.QUIP_PTB_SCORE), 0) QUIP_PTB_SCORE,
    coalesce(min(AccountIntelligence.QUIP_PTB_SEGMENT), 0) QUIP_PTB_SEGMENT,
    coalesce(min(AccountIntelligence.SOCIAL_STUDIO_ORDER_QTY), 0) SOCIAL_STUDIO_ORDER_QTY
from
    bds_ops.uf_fmo_details master

    left join Account on master.acct_id = Account.accountid

    left join MgmtChain on master.ACCT_OWN_ID = MgmtChain.userid

    left join AETable ECS_TABLE ON master.acct_id = ECS_TABLE.accountid

    left join AccountDetail on master.acct_id = AccountDetail.accountid

    left join AccountIntelligence on master.acct_id = AccountIntelligence.accountid

    left join PTBOrderQuantities SalesCloud on master.acct_id = SalesCloud.accountid
    and SalesCloud.product = 'Sales Cloud'

    left join PTBOrderQuantities ServiceCore on master.acct_id = ServiceCore.accountid
    and ServiceCore.product = 'Service Core'

    left join PTBOrderQuantities FinancialService on master.acct_id = FinancialService.accountid
    and FinancialService.product = 'Financial Services Cloud'

    left join PTBOrderQuantities HealthCloud on master.acct_id = HealthCloud.accountid
    and HealthCloud.product = 'Health Cloud'

    left join PTBOrderQuantities LightninghtPlatform on master.acct_id = LightninghtPlatform.accountid
    and LightninghtPlatform.product = 'Core Platform'

    left join PTBOrderQuantities ManufacturingCloud on master.acct_id = ManufacturingCloud.accountid
    and ManufacturingCloud.product = 'Manufacturing Cloud'

    left join PTBOrderQuantities Quip on master.acct_id = Quip.accountid
    and Quip.product = 'Quip'
where
    master.acct_id in (select accountid from PlacematAccounts)
group by
    master.acct_id
