import json as js
import os
import sys
from datetime import datetime

import numpy as np
import pandas as pd
import pytz

from PlacematFY22_Commerce import FY22_Commerce
from PlacematFY22_Marketing import FY22_Marketing
from account_detail_dups_fixer import fix_detail
from hls_extractor_multithreading import query_azprd
from utils import get_numeric_field, progress_bar_with_time


def current_time():
    utc = pytz.utc
    return datetime.now(tz=utc).strftime("%Y-%m-%d %H.%M.%S%z")


def current_date():
    utc = pytz.utc
    return datetime.now(tz=utc).strftime("%Y-%m-%d")


def run_builder(builder_metadata: js = None):
    # Gets required paths for reports-joined file and final csv file.
    extended = builder_metadata['org62-reports-joined-path'].replace('<x>', current_date())
    extended = os.path.join(os.path.dirname(__file__), extended)
    final_csv_path = builder_metadata['output-csv-path'].replace('<x>', current_time())

    # Executes query against AZPRD
    line = query_azprd(builder_metadata['azprd-sql-files'], account_intelligence=False)

    # If the reports-joined csv doesn't exists
    if not os.path.isfile(extended):
        accounts = None
        intelligences = None

        # Loads Accounts
        with progress_bar_with_time(prefix="Loading Accounts", line=line, clear=False):
            for nth in range(builder_metadata['accounts']['amount']):
                filename = builder_metadata['accounts']['filename'].replace('<x>', str(nth + 1))
                if accounts is None:
                    accounts = pd.read_csv(filename, dtype='unicode')
                    columns = accounts.columns.tolist()
                else:
                    accounts = pd.concat([accounts, pd.read_csv(filename, dtype='unicode')[columns]])
            accounts = accounts.drop_duplicates().reset_index(drop=True)

        # Fixes removing unneeded backslash character in the description field for the account "001000000000JBz"
        line += 1
        with progress_bar_with_time(prefix="Fixing Account '' description field", line=line, clear=False):
            fixed_value = accounts.loc[accounts["Account ID"] == "001000000000JBz", "Description"].values[0].replace(
                '\\', '')
            accounts.loc[accounts['Account ID'] == "001000000000JBz", 'Description'] = fixed_value

        # Loads Intelligences
        line += 1
        with progress_bar_with_time(prefix="Loading Intelligences", line=line, clear=False):
            for nth in range(builder_metadata['intelligences']['amount']):
                filename = builder_metadata['intelligences']['filename'].replace('<x>', str(nth + 1))
                if intelligences is None:
                    intelligences = pd.read_csv(filename, dtype='unicode')
                    columns = intelligences.columns.tolist()
                else:
                    intelligences = pd.concat([intelligences, pd.read_csv(filename, dtype='unicode')[columns]])
            intelligences = intelligences.drop_duplicates().reset_index(drop=True)

        # Loads Details
        line += 1
        with progress_bar_with_time(prefix="Loading Details", line=line, clear=False):
            details = pd.read_csv(builder_metadata['details']['filename'], dtype='unicode')

        # Fixes Multi Details issue
        line += 1
        with progress_bar_with_time(prefix="Fixing multi detail issue", line=line, clear=False):
            detail_reference = pd.read_csv(builder_metadata['detail-fixer']['file'], dtype='unicode')
            details_fixed = fix_detail(to_fix=details, fix_with=detail_reference)

        # Left-join with Details
        line += 1
        with progress_bar_with_time(prefix="Left-joining with Fixed Details", line=line, clear=False):
            dataset = pd.merge(accounts, details_fixed, left_on=builder_metadata['details']['left_on'],
                               right_on=builder_metadata['details']['right_on'],
                               how=builder_metadata['details']['join_method'],
                               suffixes=tuple(builder_metadata['details']['suffixes']),
                               indicator=builder_metadata['details']['indicator'])

        # Left-join with Intelligences
        line += 1
        with progress_bar_with_time(prefix="Left-joining with Intelligences", line=line, clear=False):
            dataset = pd.merge(dataset, intelligences, left_on=builder_metadata['intelligences']['left_on'],
                               right_on=builder_metadata['intelligences']['right_on'],
                               how=builder_metadata['intelligences']['join_method'],
                               suffixes=tuple(builder_metadata['details']['suffixes']),
                               indicator=builder_metadata['intelligences']['indicator'])

        # Extends with AZPRD columns
        if '--no-azprd' not in sys.argv:
            line += 1
            with progress_bar_with_time(prefix="Extending with azprd columns", line=line, clear=False):
                azprd_metadata = builder_metadata['azprd-metadata']
                for metadata in azprd_metadata:
                    if metadata['columns']:
                        file = pd.read_csv(metadata['filename'], usecols=metadata['columns'], dtype='unicode')
                    else:
                        file = pd.read_csv(metadata['filename'], dtype='unicode')

                    file = file.rename(columns=metadata['rename-columns'])
                    dataset = pd.merge(dataset, file, left_on='Account ID', right_on='Account ID', how='left',
                                       indicator=False)

        # Replaces Numpy NaN value to zero
        line += 1
        with progress_bar_with_time(prefix="Replacing Numpy NaN values to zeros", line=line, clear=False):
            columns = get_numeric_field(dataset)
            dataset[columns] = dataset[columns].replace(np.nan, 0)

        # Saves the Dataset to file
        line += 1
        with progress_bar_with_time(prefix="Saving dataset", line=line, clear=False):
            dataset.to_csv(extended, index=False)
    # If the reports-joined csv doesn't exists
    else:
        # Load dataset
        line += 0 if not line else 3
        with progress_bar_with_time(prefix="Loading Dataset", line=line, clear=False):
            dataset = pd.read_csv(extended, dtype='unicode')

    line += 2
    # Generating plays for Commerce
    with progress_bar_with_time(prefix="Generating plays for FY22 Commerce", line=line, clear=False):
        fy22_commerce = FY22_Commerce(dataset)
        dataset = fy22_commerce.generate_plays()

    # Generating plays for Marketing
    line += 1
    with progress_bar_with_time(prefix="Generating plays for FY22 Marketing", line=line, clear=False):
        fy22_marketing = FY22_Marketing(dataset)
        dataset = fy22_marketing.generate_plays()

    # Saves the Dataset to file
    line += 1
    with progress_bar_with_time(prefix="Saving dataset", line=line, clear=False):
        dataset.to_csv(final_csv_path, index=False)


if __name__ == "__main__":
    metadata_file = open('input/azprd-to-digital360.json')
    metadata = js.load(metadata_file)

    run_builder(metadata)

    metadata_file.close()
