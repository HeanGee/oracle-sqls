with
-------Found in AccInt or AccDetails queries
    Details as (
          select AccountDetails.ACCOUNTID
          from ODS.ODS_ACCOUNT_DETAIL__C AccountDetails
          where AccountDetails.DEL_FLG = 'N'
          group by AccountDetails.ACCOUNTID
    ),

    Intelligence as (
        select AccountInt.ACCOUNT__C
        from ODS.ODS_ACCOUNT_INTELLIGENCE__C AccountInt
        where AccountInt.DEL_FLG = 'N'
        group by AccountInt.ACCOUNT__C
    ),
    
------Asset info subqueries
    Sandbox as (
        select
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) Sandbox
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Full Copy Sandbox%' and name like '%(Full Copy)%' or name like '%(Partial Copy)%' or name like '%(Developer Pro)%'
        and APTTUS_CONFIG2__ISINACTIVE__C = 'false'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    DataMask as (
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) Data_Mask
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Data Mask%'
        and APTTUS_CONFIG2__ISINACTIVE__C = 'false'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    SCEinstein as (
       --Check for Service Cloud Einstein
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) Service_Cloud_Einstein
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Service Cloud Einstein%'
        and APTTUS_CONFIG2__ISINACTIVE__C = 'false'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    SCVoice as (
       --Check for Service Cloud Einstein
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) Service_Cloud_Voice
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Service Cloud Voice%'
        and APTTUS_CONFIG2__ISINACTIVE__C = 'false'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    Shield as (
        --Check for Shield
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) Shield
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like 'Salesforce Shield%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    EventMonitoring as (
        --Check for EventMonitoring
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) EventMonitoring
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Event Monitoring%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    FieldAuditTrail as (
        --Check for Field Audit Trail
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) FieldAuditTrail
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Field Audit Trail%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
       ),

    PlatformEncryption as (
        --Check for Encryption
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) PlatformEncryption
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Platform Encryption%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    CPQPlus as (
        --Check for Encryption
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) CPQPlus
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%CPQ+%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),
        
    Billing as (
        --Check for Encryption
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) Billing
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Billing%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),
        
    AssetScheduling as (
        --Check for Encryption
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) Asset_Scheduling_Ord_Qty
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Asset Scheduling%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),
        
    VisualRemoteAssistant as (
        --Check for Encryption
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) V_R_Assist_Ord_Qty
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Visual Remote Assistant%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),
        
    LightningScheduler as (
        --Check for Encryption
        select 
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,     
            sum(APTTUS_CONFIG2__QUANTITY__C) Light_Scheduler_Ord_Qty
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Lightning Scheduler%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    FieldSrvContractor as (
        --Check for Encryption
        select
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,
            sum(APTTUS_CONFIG2__QUANTITY__C) FSrv_Contractor_Ord_Qty
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and (name like '%Field Service Contractor%' or name like '%Contractor+%')
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    SuccessPlan as (
        --Check for Encryption
        select
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,
            sum(APTTUS_CONFIG2__QUANTITY__C) Success_Plan_Ord_Qty
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Success Plan%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),

    MobilePubs as (
        --Check for Encryption
        select
            APTTUS_CONFIG2__ACCOUNTID__C   AccountID,
            sum(APTTUS_CONFIG2__QUANTITY__C) ord_qty
        from
            ODS.ODS_APTTUS_ASSETLINEITEM AssetLine
        where APTTUS_CONFIG2__QUANTITY__C is not null and DEL_FLG = 'N' and ISDELETED = 'false'
        and name like '%Mobile Publisher%'
        group by APTTUS_CONFIG2__ACCOUNTID__C--, name
        ),
------------AE Subqueries
    QuipInfo as (
        select
            account.ID_15 account_id,
            quip_ae.name quip_ae,
            manager.name quip_manager,
            teams.Name role
        from ODS.ODS_ACCOUNT account

            left join ODS.ODS_SFBASE__SALESFORCETEAM__C sfteam
            on account.ID_15 = sfteam.SFBASE__ACCOUNT__C

            left join ODS.ODS_USER quip_ae
            on quip_ae.ID_15 = sfteam.sfbase__user__c

            left join ODS.ODS_USER manager
            on manager.ID = quip_ae.managerid

            left join ODS.ODS_TEAM_ROLE__C teams
            on sfteam.TEAM_ROLE__C = teams.ID_15

        where teams.Name = 'Quip AE'
        and sfteam.SFBASE__ENDDATE__C is null
        and sfteam.SFBASE__COMPVALIDATIONROLE__C = 'Quip AE'
        and sfteam.TERRITORY__C is not null
        and sfteam.ISDELETED <> 'true'
        ),

    AETable as (
        select
            Teamtable.SFBASE__ACCOUNT__C account_id,
            TableAE.Name AE_Name,
            TeamRole.Name AE_Role
        from ODS.ODS_SFBASE__SALESFORCETEAM__C Teamtable
        left join ODS.ODS_USER TableAE
            on TableAE.ID_15 = Teamtable.sfbase__user__c
        left join ODS.ODS_TEAM_ROLE__C TeamRole
            on Teamtable.TEAM_ROLE__C = TeamRole.ID_15
        where TeamRole.Name in ('B2B Commerce AE', 'B2C Commerce AE','Commerce Cloud AE')
            and Teamtable.SFBASE__ENDDATE__C is null
            and Teamtable.TERRITORY__C is not null
            and Teamtable.ISDELETED <> 'true'
            and Teamtable.DEL_FLG = 'N'
        ),

    CasesLast7Days as (
        select account_id, max(NUM_CASES_CREATED_IN_LAST_7D) "Cases Last 7 Days"
        from ods_s.ods_core_org_summary
        where qry_date=to_date('29-Jan-2021') and account_id<>'000000000000000'
        and (ACCOUNT_ID,last_login) in
        (
            select ACCOUNT_ID,max(last_login)
            from ods_s.ods_core_org_summary
            where qry_date=to_date('29-Jan-2021')
            and account_id<>'000000000000000'
            group by ACCOUNT_ID
        )
        group by account_id
    )
----- MAIN

select
    AccountT.ID_15 as "AccountID",
    --AccountT.DEL_FLG,
    AccountT.COMPANY__C as "Company",
    AccountT.GLOBAL_COMPANY__C as "Global Company",
    -----------INPUT FIELDS----------
    AccountT.TOTAL_AOV__C as "Total AOV",
    AccountT.L1_AOV_Cloud3__c as "L1 Marketing Cloud AOV",
    AccountT.L1_AOV_Cloud1__c as "L1 Sales Cloud AOV",
    AccountT.L1_AOV_Cloud2__c as "L1 Service Cloud AOV",
    AccountT.M_A__C  as M_A,
    
    ---------------------
    --AccountT.NAME ,  
    --UserT.NAME As "OwnerName",
    --UserT.USERROLEID,
    --RoleT.NAME As "Owner Role",
    --UserT.MGMT_CHAIN_LEVEL_06__C,
    --UsrLvl6.NAME AS "Level 6 Name",
    --UsrLvl6.USERROLEID As "Lvl6 RoleID",
    lvl6Role.NAME                   as "Account Level 6 User Role",
    --UserT.MGMT_CHAIN_LEVEL_07__C,
    --UsrLvl7.USERROLEID As "Lvl7 RoleID",
    lvl7Role.NAME                   as "Account Level 7 User Role",
    (case  when AccountT.COMPANY__C is null then GlobalCompanyT.LOCKED_INDUSTRY__C else CompanyT.LOCKED_INDUSTRY__C  end) as "Combo Industry",
    (case  when AccountT.COMPANY__C is null then GlobalCompanyT.LOCKED_SECTOR__C else CompanyT.LOCKED_SECTOR__C  end) as "Combo Sector",
    (case  when AccountT.COMPANY__C is null then GlobalCompanyT.LOCKED_SUB_SECTOR__C else CompanyT.LOCKED_SUB_SECTOR__C  end) as "Combo Sub-Sector",
    CompanyT.LOCKED_INDUSTRY__C     as "COMPANY Locked Industry",
    CompanyT.LOCKED_SECTOR__C       as "COMPANY Locked Sector",
    CompanyT.LOCKED_SUB_SECTOR__C   as "COMPANY Locked Sub-Sector",
    AccountT.CKZ_AOV__C as "B2C_CommerceAOV",
    case when CountingT.acctCount > 1 or GCountingT.GacctCount>1  then 'Yes' else 'No' end MultiOrg,
    Shield.Shield,
    EventMonitoring.EventMonitoring,
    FieldAuditTrail.FieldAuditTrail,
    PlatformEncryption.PlatformEncryption,
    CPQPlus.CPQPlus,
    Billing.Billing,
    SCEinstein.Service_Cloud_Einstein,
    SCVoice.Service_Cloud_Voice,
    DataMask.Data_Mask,
    Sandbox.Sandbox,
    case
    when AccountT.REGION__C = 'AMER' and (AccountT.TERRITORY__C like '%_CBU_GEN_%' or AccountT.TERRITORY__C like '%GEN%' or AccountT.TERRITORY__C like '%_CBU_CMP_%' or AccountT.TERRITORY__C like '%_B2C_RCG_CMRCL_MID_%' or AccountT.TERRITORY__C in ('AMER_C_B2C_RCG_CG_E_GEN_','AMER_C_B2C_RCG_CG_W_GEN_','AMER_C_B2C_RCG_RETAIL_KSG_E_GEN_','AMER_C_B2C_RCG_RETAIL_KSG_W_GEN_') or AccountT.TERRITORY__C like '%AMER_C_B2C_RCG_RETAIL_KSG_C_GEN_C_%') then 'CMRCL'
    when AccountT.REGION__C = 'AMER' and (AccountT.TERRITORY__C like '%_VERTS_HLS_CMRCL_%' or AccountT.TERRITORY__C like '%_VERTS_FINS_CMRCL_%') then 'CMRCL'
    when AccountT.REGION__C = 'AMER' and (AccountT.TERRITORY__C like '%_B2C_RCG_CMRCL_SMB_GRB_%' or AccountT.TERRITORY__C like '%_CAN_CMRCL_%' or AccountT.TERRITORY__C like '%_CAN_SMB_%' or AccountT.TERRITORY__C like '%_CBU_MGM_MID_%' or AccountT.TERRITORY__C like '%_CBU_MGM_MFG_%' or (AccountT.TERRITORY__C like '%_CBU_SMB_%' and AccountT.TERRITORY__C like '%GRB%') or AccountT.TERRITORY__C like '%AMER_C_CBU_MGM_NewLogo_OC%') then 'CMRCL'
    when AccountT.REGION__C = 'AMER' and ((AccountT.TERRITORY__C like '%_CBU_SMB_%' and AccountT.TERRITORY__C like '%SB%') or AccountT.TERRITORY__C like '%ESSENTIALS%') or AccountT.TERRITORY__C like '%_B2C_RCG_CMRCL_SMB_SB_%' then 'CMRCL'
    when AccountT.REGION__C = 'AMER' and (AccountT.TERRITORY__C like '%_STRAT_NE_%' or AccountT.TERRITORY__C like '%_STRAT_PROSERV_%' or (AccountT.TERRITORY__C like '%_STRAT_CEN-SE_%' and not AccountT.TERRITORY__C like '%_STRAT_CEN-SE_2_%') or AccountT.TERRITORY__C like '%_KEY_EAST_%' or AccountT.TERRITORY__C like '%_SEL_E_%' or AccountT.TERRITORY__C like '%_CAN_STRAT_%' or AccountT.TERRITORY__C like '%PS%') then 'EBU'
    when AccountT.REGION__C = 'AMER' and (AccountT.TERRITORY__C like '%_STRAT_W%' or AccountT.TERRITORY__C like '%_STRAT_SW_%' or AccountT.TERRITORY__C like '%_STRAT_CEN-SE_2_%' or AccountT.TERRITORY__C like '%_STRAT_ENERGY_%' or AccountT.TERRITORY__C like '%_STRAT_NW_%' or AccountT.TERRITORY__C like '%_KEY_WC_%' or AccountT.TERRITORY__C like '%_KEY_WEST_%' or AccountT.TERRITORY__C like '%_SEL_W_%' or AccountT.TERRITORY__C like '%_SEL_C_%' or (AccountT.TERRITORY__C like '%_B2C_RCG_%' and not AccountT.TERRITORY__C like '%CMRCL%' and not AccountT.TERRITORY__C like '%GEN%')) then 'EBU'
    when AccountT.REGION__C = 'AMER' and (AccountT.TERRITORY__C like '%VERTS%' and not AccountT.TERRITORY__C like '%CMRCL%') then 'EBU'
    when AccountT.REGION__C = 'EMEA' then 'INTL'
    when AccountT.REGION__C = 'APAC' then 'INTL'
    when AccountT.REGION__C = 'JP' then 'INTL'
    when AccountT.REGION__C = 'LACA' then 'INTL'
    else 'CMRCL'
    end Quip_Segment,
    QuipInfo.quip_ae,
    AssetScheduling.Asset_Scheduling_Ord_Qty,
    VisualRemoteAssistant.V_R_Assist_Ord_Qty,
    LightningScheduler.Light_Scheduler_Ord_Qty,
    FieldSrvContractor.FSrv_Contractor_Ord_Qty,
    SuccessPlan.Success_Plan_Ord_Qty,
    MobilePubs.ord_qty as "Mobile Publisher Order Qty",
    CasesLast7Days."Cases Last 7 Days" as "Cases Last 7 Days"

from ODS.ODS_ACCOUNT AccountT
inner join Details AccountDetails
on AccountT.ID = AccountDetails.ACCOUNTID
inner join Intelligence AccountInt
on AccountT.ID_15 = AccountInt.ACCOUNT__C
left join ODS.ODS_USER UserT
on AccountT.OWNERID = UserT.ID_15
left join ODS.ODS_USERROLE RoleT
on UserT.USERROLEID = RoleT.ID_15
left join ODS.ODS_USER UsrLvl6
on UserT.MGMT_CHAIN_LEVEL_06__C = UsrLvl6.ID
left join ODS.ODS_USERROLE lvl6Role
on UsrLvl6.USERROLEID = lvl6Role.ID_15
left join ODS.ODS_USER UsrLvl7
on UserT.MGMT_CHAIN_LEVEL_07__C = UsrLvl7.ID
left join ODS.ODS_USERROLE lvl7Role
on UsrLvl6.USERROLEID = lvl7Role.ID_15
left join ODS.ODS_COMPANY__C CompanyT
on CompanyT.ID_15 = AccountT.COMPANY__C
left join ODS.ODS_COMPANY__C GlobalCompanyT
on GlobalCompanyT.ID_15 = AccountT.GLOBAL_COMPANY__C
left join (
    select
        AccountT.COMPANY__C company,
        count(accountT.ID) acctCount
    from ODS.ODS_ACCOUNT AccountT
    where AccountT.DEL_FLG like 'N'
    and AccountT.COMPANY__C is not null
    group by AccountT.COMPANY__C
    ) CountingT
on  AccountT.COMPANY__C = CountingT.company
left join (
    select
    AccountT.global_company__c Gcompany,
    count(accountT.ID) GacctCount
    from ODS.ODS_ACCOUNT AccountT
    where AccountT.DEL_FLG like 'N'
    --AND AccountT.global_company__c IS NOT null
    group by AccountT.global_company__c) GCountingT
on  AccountT.global_company__c = GCountingT.Gcompany
---- Assset info
left join Shield          on AccountT.ID_15 =  Shield.AccountID -- AND Product1 -
left join EventMonitoring on AccountT.ID_15 =  EventMonitoring.AccountID -- AND Product2
left join FieldAuditTrail on AccountT.ID_15 =  FieldAuditTrail.AccountID
left join PlatformEncryption  on AccountT.ID_15 =  PlatformEncryption.AccountID
left join SCEinstein  on AccountT.ID_15 =  SCEinstein.AccountID
left join SCVoice  on AccountT.ID_15 =  SCVoice.AccountID
left join CPQPlus  on AccountT.ID_15 =  CPQPlus.AccountID
left join Billing  on AccountT.ID_15 =  Billing.AccountID
left join QuipInfo on AccountT.ID_15 =  QuipInfo.account_id
left join DataMask on AccountT.ID_15 =  DataMask.AccountID
left join Sandbox on AccountT.ID_15 =  Sandbox.AccountID
left join AssetScheduling on AccountT.ID_15 = AssetScheduling.AccountID
left join VisualRemoteAssistant on AccountT.ID_15 = VisualRemoteAssistant.AccountID
left join LightningScheduler on AccountT.ID_15 = LightningScheduler.AccountID
left join FieldSrvContractor on AccountT.ID_15 = FieldSrvContractor.AccountID
left join SuccessPlan on AccountT.ID_15 = SuccessPlan.AccountID
left join MobilePubs on AccountT.ID_15 = MobilePubs.AccountID
left join CasesLast7Days on AccountT.ID_15 = CasesLast7Days.account_id
where AccountT.DEL_FLG like 'N'
-------------------

