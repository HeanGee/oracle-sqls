WITH
    ACCTable AS (
        SELECT
            AccountT.ID_15  AccountID
        FROM ODS.ODS_ACCOUNT AccountT
        WHERE AccountT.DEL_FLG = 'N'
            AND AccountT.ID IN (SELECT AccountDetails.ACCOUNTID FROM ODS.ODS_ACCOUNT_DETAIL__C AccountDetails WHERE DEL_FLG = 'N')
            AND AccountT.ID_15 IN (SELECT AccountIntelligence.ACCOUNT__C FROM ODS.ODS_ACCOUNT_INTELLIGENCE__C AccountIntelligence WHERE DEL_FLG = 'N')
    ),

    Roles as (
        SELECT
            Teamtable.SFBASE__ACCOUNT__C AccountID,
            TableAE.Name AE,
            TableAEM.name Mngr,
            ManagerOfAEManager.name Mngr2,
            TeamRole.Name Role
        FROM ODS.ODS_SFBASE__SALESFORCETEAM__C Teamtable
            -- Limits the accounts to those for AccountIntelligence.
            INNER JOIN ACCTable
            ON  ACCTable.AccountID = Teamtable.SFBASE__ACCOUNT__C

            -- Getting Account Executives.
            LEFT JOIN ODS.ODS_USER TableAE
            ON TableAE.ID_15 = Teamtable.sfbase__user__c

            -- Getting Managers of AEs.
            LEFT JOIN ODS.ODS_USER TableAEM
            ON TableAEM.ID = TableAE.managerid

            -- Getting Managers of AEs Managers.
            LEFT JOIN ODS.ODS_USER ManagerOfAEManager
            ON ManagerOfAEManager.ID = TableAEM.managerid

            -- Getting Roles.
            LEFT JOIN ODS.ODS_TEAM_ROLE__C TeamRole
            ON TeamRole.ID_15 = Teamtable.TEAM_ROLE__C

        WHERE TeamRole.Name in (
                'Analytics AE', 'B2B Commerce AE', 'B2B Commerce BDR', 'B2C Commerce AE', 'BDR', 'CPQ AE', 'Commerce Cloud AE', 'Commerce Cloud BDR', 'DTR Agency AE', 'DTR BDR', 'DTR Brand AE', 'ECS', 'MC Ads AE', 'MC BDR', 'MC DMP AE', 'MC Social AE', 'Map AE', 'Pardot AE', 'Pardot BDR', 'Platform AE', 'Quip AE', 'Regional MC AE', 'Salesperson'
            )
            AND Teamtable.SFBASE__ENDDATE__C is null
            AND Teamtable.TERRITORY__C is not null
            AND Teamtable.ISDELETED <> 'true'
            AND Teamtable.DEL_FLG = 'N'
    )

select
    Account.AccountID,
    Analytics.AE AnalyticsAE, Analytics.Mngr AnalyticsMngr, Analytics.Mngr2 AnalyticsMngr2,
    B2BComm.AE B2BCommAE, B2BComm.Mngr B2BCommMngr, B2BComm.Mngr2 B2BCommMngr2
from
    ACCTable Account
left join Roles Analytics on Account.AccountID = Analytics.AccountID and Analytics.Role = 'Analytics AE'
left join Roles B2BComm on Account.AccountID = B2BComm.AccountID and B2BComm.Role = 'B2B Commerce AE'

