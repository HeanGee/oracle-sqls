SELECT 
    AccountT.GLOBAL_COMPANY__C as "Global Company ID",
    SUM(accountT.LOCKED_EMPLOYEES__C) as "Emp Count"
FROM
    ODS.ODS_ACCOUNT AccountT 
WHERE
    AccountT.DEL_FLG LIKE 'N'
    AND AccountT.GLOBAL_COMPANY__C IS NOT null
GROUP BY
    AccountT.GLOBAL_COMPANY__C

