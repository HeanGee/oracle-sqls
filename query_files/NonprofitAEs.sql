WITH PlacematAccounts   AS (
        SELECT SUBSTR(AccountDetails.accountid, 0, 15) accountid
        FROM ODS.ODS_ACCOUNT_DETAIL__C AccountDetails
        WHERE AccountDetails.DEL_FLG = 'N'
        union
        SELECT AccountInt.ACCOUNT__C accountid
        FROM ODS.ODS_ACCOUNT_INTELLIGENCE__C AccountInt
        WHERE AccountInt.DEL_FLG = 'N'
    )

select
    AccountT.id_15 ACCOUNTID,
    UserT.Name NONPROFIT_RVP
from
    ods.ods_account AccountT

    left join ODS.ODS_USER UserT
    on AccountT.OWNERID = UserT.ID_15

    left join ODS.ODS_USERROLE RoleT
    on UserT.USERROLEID = RoleT.ID_15
where 1=1
    AND accountt.id_15 in (select accountid from PlacematAccounts)
    and (rolet.name like '%AMER - NP - MC%' or rolet.name like '%AMER - NP - NGO%' or rolet.name like '%APAC - NP - ANZ - NGO%'
        or rolet.name like '%EMEA - NP - AVP%' or rolet.name like '%EMEA - NP - MC%' or rolet.name like '%EMEA - NP - NGO%')
