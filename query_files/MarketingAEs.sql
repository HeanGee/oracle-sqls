WITH
    ACCTable AS (
        SELECT 
            AccountT.ID_15  AccountID
        FROM ODS.ODS_ACCOUNT AccountT 
        WHERE AccountT.DEL_FLG = 'N'
            AND AccountT.ID IN (SELECT AccountDetails.ACCOUNTID FROM ODS.ODS_ACCOUNT_DETAIL__C AccountDetails WHERE DEL_FLG = 'N')
            AND AccountT.ID_15 IN (SELECT AccountIntelligence.ACCOUNT__C FROM ODS.ODS_ACCOUNT_INTELLIGENCE__C AccountIntelligence WHERE DEL_FLG = 'N')
    ),

    AETable AS (
        SELECT
            Teamtable.SFBASE__ACCOUNT__C account_id,
            TableAE.Name AE_Name,
            TableAEM.name AE_Manager,
            ManagerOfAEManager.name AE_Manager_Manager,
            TeamRole.Name AE_Role
        FROM ODS.ODS_SFBASE__SALESFORCETEAM__C Teamtable
            -- Limits the accounts to those for AccountIntelligence.
            INNER JOIN ACCTable 
            ON  ACCTable.AccountID = Teamtable.SFBASE__ACCOUNT__C

            -- Getting Account Executives.
            LEFT JOIN ODS.ODS_USER TableAE 
            ON TableAE.ID_15 = Teamtable.sfbase__user__c

            -- Getting Managers of AEs.
            LEFT JOIN ODS.ODS_USER TableAEM
            ON TableAEM.ID = TableAE.managerid

            -- Getting Managers of AEs Managers.
            LEFT JOIN ODS.ODS_USER ManagerOfAEManager
            ON ManagerOfAEManager.ID = TableAEM.managerid

            -- Getting Roles.
            LEFT JOIN ODS.ODS_TEAM_ROLE__C TeamRole
            ON TeamRole.ID_15 = Teamtable.TEAM_ROLE__C
            
        WHERE TeamRole.Name in (
            'ECS', 'BDR', 'Regional MC AE', 'Pardot AE', 'DTR AE', 'MC Data and Identity AE', 'MC BDR',
            'MCandPardot org AE', 'Salesperson', 'Service Cloud AE', 'Interaction Studio AE',
            'B2B Commerce AE', 'B2C Commerce AE', 'B2C Commerce BDR', 'CSG Account Partner - Industries',
            'CSG Account Partner', 'CSG Renewals Mgr Off-Cycle', 'CSG Solution Advisor', 'CSG Success Mgr',
            'Commerce Cloud AE', 'Commerce Cloud BDR', 'GAM', 'Industries AE', 'Map AE', 'MuleSoft Account Development',
            'MuleSoft Composer AE', 'Philanthropy Cloud AE', 'Platform AE', 'Quip AE', 'Quip BDR', 'TAE',
            'Tableau AE', 'myTrailhead AE'
        )
        AND Teamtable.SFBASE__ENDDATE__C is null
        AND Teamtable.TERRITORY__C is not null
        AND Teamtable.ISDELETED <> 'true'
        AND Teamtable.DEL_FLG = 'N'
    )

SELECT
    ACCTable.AccountID AccountID,
    
    SALESPERSON_TABLE.AE_Name SALESPERSON_AE,
    SALESPERSON_TABLE.AE_Manager SALESPERSON_MANAGER,
    SALESPERSON_TABLE.AE_Manager_Manager SALESPERSON_MANAGER_M,
    
    SRV_CL_TABLE.AE_Name SRVCL_AE,
    SRV_CL_TABLE.AE_Manager SRVCL_MANAGER,
    SRV_CL_TABLE.AE_Manager_Manager SRVCL_MANAGER_M,
    
    REG_MC_TABLE.AE_Name REG_MC_AE,
    REG_MC_TABLE.AE_Manager REG_MC_MANAGER,
    REG_MC_TABLE.AE_Manager_Manager REG_MC_MANAGER_M,

    PARDOT_TABLE.AE_Name PARDOT_AE,
    PARDOT_TABLE.AE_Manager PARDOT_MANAGER,
    PARDOT_TABLE.AE_Manager_Manager PARDOT_MANAGER_M,

    DTR_TABLE.AE_Name DTR_AE,
    DTR_TABLE.AE_Manager DTR_MANAGER,
    DTR_TABLE.AE_Manager_Manager DTR_MANAGER_M,

    MC_DATA_TABLE.AE_Name MC_DATA_AE,
    MC_DATA_TABLE.AE_Manager MC_DATA_MANAGER,
    MC_DATA_TABLE.AE_Manager_Manager MC_DATA_MANAGER_M,

    ECS_TABLE.AE_Name ECS_AE,
    ECS_TABLE.AE_Manager ECS_MANAGER,
    ECS_TABLE.AE_Manager_Manager ECS_MANAGER_M,

    BDR_TABLE.AE_Name BDR_AE,
    BDR_TABLE.AE_Manager BDR_MANAGER,
    BDR_TABLE.AE_Manager_Manager BDR_MANAGER_M,

    MC_BDR.AE_Name MC_BDR_AE,
    MC_BDR.AE_Manager MC_BDR_MANAGER,
    MC_BDR.AE_Manager_Manager MC_BDR_MANAGER_M,

    MCandPardot.AE_Name MCandPardot_AE,
    MCandPardot.AE_Manager MCandPardot_MANAGER,
    MCandPardot.AE_Manager_Manager MCandPardot_Manager_M,

    InteractionS.AE_Name InteractionS_AE,
    InteractionS.AE_Manager InteractionS_MANAGER,
    InteractionS.AE_Manager_Manager InteractionS_Manager_M,

    B2BTable.AE_Name  B2B_AE,
    B2BTable.AE_Manager  B2B_MANAGER,
    B2BTable.AE_Manager_Manager B2B_MANAGER_M,

    B2CTable.AE_Name  B2C_AE,
    B2CTable.AE_Manager  B2C_MANAGER,
    B2CTable.AE_Manager_Manager B2C_MANAGER_M,

    CMRCTable.AE_Name CMRC_AE,
    CMRCTable.AE_Manager CMRC_MANAGER,
    CMRCTable.AE_Manager_Manager CMRC_MANAGER_M,

    B2CBTatble.AE_Name B2C_BDR_AE,
    B2CBTatble.AE_Manager B2C_BDR_MANAGER,
    B2CBTatble.AE_Manager_Manager B2C_BDR_MANAGER_M,

    CSGAPITable.AE_Name GSG_ACC_PART_IND_AE,
    CSGAPITable.AE_Manager GSG_ACC_PART_IND_MANAGER,
    CSGAPITable.AE_Manager_Manager GSG_ACC_PART_IND_MANAGER_M,

    CSGAP.AE_Name CSG_ACC_PART_AE,
    CSGAP.AE_Manager CSG_ACC_PART_MANAGER,
    CSGAP.AE_Manager_Manager CSG_ACC_PART_MANAGER_M,

    CSGRMOCTable.AE_Name CSG_RENEW_MGR_OFFC_AE,
    CSGRMOCTable.AE_Manager CSG_RENEW_MGR_OFFC_MANAGER,
    CSGRMOCTable.AE_Manager_Manager CSG_RENEW_MGR_OFFC_MANAGER_M,

    CSGSATable.AE_Name CSG_SOL_ADV_AE,
    CSGSATable.AE_Manager CSG_SOL_ADV_MANAGER,
    CSGSATable.AE_Manager_Manager CSG_SOL_ADV_MANAGER_M,

    CSGSMTable.AE_Name CSG_SUCC_MRG_AE,
    CSGSMTable.AE_Manager CSG_SUCC_MRG_MANAGER,
    CSGSMTable.AE_Manager_Manager CSG_SUCC_MRG_MANAGER_M,

    COMMCTable.AE_Name COMM_CL_AE,
    COMMCTable.AE_Manager COMM_CL_MANAGER,
    COMMCTable.AE_Manager_Manager COMM_CL_MANAGER_M,

    COMMCBTable.AE_Name COMM_CL_BDR_AE,
    COMMCBTable.AE_Manager COMM_CL_BDR_MANAGER,
    COMMCBTable.AE_Manager_Manager COMM_CL_BDR_MANAGER_M,

    GAMTable.AE_Name GAM_AE,
    GAMTable.AE_Manager GAM_MANAGER,
    GAMTable.AE_Manager_Manager GAM_MANAGER_M,

    INDTable.AE_Name INDU_AE,
    INDTable.AE_Manager INDU_MANAGER,
    INDTable.AE_Manager_Manager INDU_MANAGER_M,

    MAPTable.AE_Name MAP_AE,
    MAPTable.AE_Manager MAP_MANAGER,
    MAPTable.AE_Manager_Manager MAP_MANAGER_M,

    MADTable.AE_Name MULESOFT_ACC_DEV_AE,
    MADTable.AE_Manager MULESOFT_ACC_DEV_MANAGER,
    MADTable.AE_Manager_Manager MULESOFT_ACC_DEV_MANAGER_M,

    MCAETable.AE_Name MULESOFT_COMP_AE,
    MCAETable.AE_Manager MULESOFT_COMP_MANAGER,
    MCAETable.AE_Manager_Manager MULESOFT_COMP_MANAGER_M,

    PCATable.AE_Name PHILANTROPIC_AE,
    PCATable.AE_Manager PHILANTROPIC_MANAGER,
    PCATable.AE_Manager_Manager PHILANTROPIC_MANAGER_M,

    PLATFTable.AE_Name PLATFORM_AE,
    PLATFTable.AE_Manager PLATFORM_MANAGER,
    PLATFTable.AE_Manager_Manager PLATFORM_MANAGER_M,

    QUIPTable.AE_Name QUIP_AE,
    QUIPTable.AE_Manager QUIP_MANAGER,
    QUIPTable.AE_Manager_Manager QUIP_MANAGER_M,

    QUIPBTable.AE_Name QUIP_BDR_AE,
    QUIPBTable.AE_Manager QUIP_BDR_MANAGER,
    QUIPBTable.AE_Manager_Manager QUIP_BDR_MANAGER_M,

    TAETable.AE_Name TAE_AE,
    TAETable.AE_Manager TAE_MANAGER,
    TAETable.AE_Manager_Manager TAE_MANAGER_M,

    TABLEAUTable.AE_Name TABLEAU_AE,
    TABLEAUTable.AE_Manager TABLEAU_MANAGER,
    TABLEAUTable.AE_Manager_Manager TABLEAU_MANAGER_M,

    TRAILHEADTable.AE_Name TRAILHEAD_AE,
    TRAILHEADTable.AE_Manager TRAILHEADMANAGER,
    TRAILHEADTable.AE_Manager_Manager TRAILHEADMANAGER_M

FROM ACCTable

    LEFT JOIN AETable SALESPERSON_TABLE
    ON ACCTable.AccountID = SALESPERSON_TABLE.account_id AND SALESPERSON_TABLE.AE_Role = 'Salesperson'

    LEFT JOIN AETable SRV_CL_TABLE
    ON ACCTable.AccountID = SRV_CL_TABLE.account_id AND SRV_CL_TABLE.AE_Role = 'Service Cloud AE'

    LEFT JOIN AETable REG_MC_TABLE
    ON ACCTable.AccountID = REG_MC_TABLE.account_id AND REG_MC_TABLE.AE_Role = 'Regional MC AE'

    LEFT JOIN AETable PARDOT_TABLE
    ON ACCTable.AccountID = PARDOT_TABLE.account_id AND PARDOT_TABLE.AE_Role = 'Pardot AE'

    LEFT JOIN AETable DTR_TABLE
    ON ACCTable.AccountID = DTR_TABLE.account_id AND DTR_TABLE.AE_Role = 'DTR AE'

    LEFT JOIN AETable MC_DATA_TABLE
    ON ACCTable.AccountID = MC_DATA_TABLE.account_id AND MC_DATA_TABLE.AE_Role = 'MC Data and Identity AE'

    LEFT JOIN AETable ECS_TABLE
    ON ACCTable.AccountID = ECS_TABLE.account_id AND ECS_TABLE.AE_Role = 'ECS'

    LEFT JOIN AETable BDR_TABLE
    ON ACCTable.AccountID = BDR_TABLE.account_id AND BDR_TABLE.AE_Role = 'BDR'

    LEFT JOIN AETable MC_BDR
    ON ACCTable.AccountID = MC_BDR.account_id AND MC_BDR.AE_Role = 'MC BDR'

    LEFT JOIN AETable MCandPardot
    ON Acctable.accountid = MCandPardot.account_id and MCandpardot.AE_Role = 'MCandPardot org AE'

    LEFT JOIN AETable InteractionS
    ON Acctable.accountid = InteractionS.account_id and InteractionS.AE_Role = 'Interaction Studio AE'
    
    LEFT JOIN AETable B2BTable
    ON ACCTABLE.AccountID = B2BTable.account_id and B2BTable.AE_Role = 'B2B Commerce AE'

    LEFT JOIN AETable B2CTable
    ON ACCTABLE.AccountID = B2CTable.account_id and B2CTable.AE_Role = 'B2C Commerce AE'

    LEFT JOIN AETable B2CBTatble
    ON ACCTABLE.AccountID = B2CBTatble.account_id and B2CBTatble.AE_Role = 'B2C Commerce BDR'

    LEFT JOIN AETable CMRCTable
    ON ACCTABLE.AccountID = CMRCTable.account_id and CMRCTable.AE_Role = 'Commerce Cloud AE'

    LEFT JOIN AETable CSGAPITable
    ON ACCTABLE.AccountID = CSGAPITable.account_id and CSGAPITable.AE_Role = 'CSG Account Partner - Industries'

    LEFT JOIN AETable CSGAP
    ON ACCTABLE.AccountID = CSGAP.account_id and CSGAP.AE_Role = 'CSG Account Partner'

    LEFT JOIN AETable CSGRMOCTable
    ON ACCTABLE.AccountID = CSGRMOCTable.account_id and CSGRMOCTable.AE_Role = 'CSG Renewals Mgr Off-Cycle'

    LEFT JOIN AETable CSGSATable
    ON ACCTABLE.AccountID = CSGSATable.account_id and CSGSATable.AE_Role = 'CSG Solution Advisor'

    LEFT JOIN AETable CSGSMTable
    ON ACCTABLE.AccountID = CSGSMTable.account_id and CSGSMTable.AE_Role = 'CSG Success Mgr'

    LEFT JOIN AETable COMMCTable
    ON ACCTABLE.AccountID = COMMCTable.account_id and COMMCTable.AE_Role = 'Commerce Cloud AE'

    LEFT JOIN AETable COMMCBTable
    ON ACCTABLE.AccountID = COMMCBTable.account_id and COMMCBTable.AE_Role = 'Commerce Cloud BDR'

    LEFT JOIN AETable GAMTable
    ON ACCTABLE.AccountID = GAMTable.account_id and GAMTable.AE_Role = 'GAM'

    LEFT JOIN AETable INDTable
    ON ACCTABLE.AccountID = INDTable.account_id and INDTable.AE_Role = 'Industries AE'

    LEFT JOIN AETable MAPTable
    ON ACCTABLE.AccountID = MAPTable.account_id and MAPTable.AE_Role = 'Map AE'

    LEFT JOIN AETable MADTable
    ON ACCTABLE.AccountID = MADTable.account_id and MADTable.AE_Role = 'MuleSoft Account Development'

    LEFT JOIN AETable MCAETable
    ON ACCTABLE.AccountID = MCAETable.account_id and MCAETable.AE_Role = 'MuleSoft Composer AE'

    LEFT JOIN AETable PCATable
    ON ACCTABLE.AccountID = PCATable.account_id and PCATable.AE_Role = 'Philanthropy Cloud AE'

    LEFT JOIN AETable PLATFTable
    ON ACCTABLE.AccountID = PLATFTable.account_id and PLATFTable.AE_Role = 'Platform AE'

    LEFT JOIN AETable QUIPTable
    ON ACCTABLE.AccountID = QUIPTable.account_id and QUIPTable.AE_Role = 'Quip AE'

    LEFT JOIN AETable QUIPBTable
    ON ACCTABLE.AccountID = QUIPBTable.account_id and QUIPBTable.AE_Role = 'Quip BDR'

    LEFT JOIN AETable TAETable
    ON ACCTABLE.AccountID = TAETable.account_id and TAETable.AE_Role = 'TAE'

    LEFT JOIN AETable TABLEAUTable
    ON ACCTABLE.AccountID = TABLEAUTable.account_id and TABLEAUTable.AE_Role = 'Tableau AE'

    LEFT JOIN AETable TRAILHEADTable
    ON ACCTABLE.AccountID = TRAILHEADTable.account_id and TRAILHEADTable.AE_Role = 'myTrailhead AE'
    
WHERE (1=1
    OR SALESPERSON_TABLE.AE_Name IS NOT NULL
    OR SRV_CL_TABLE.AE_Name IS NOT NULL
    OR REG_MC_TABLE.AE_Name IS NOT NULL
    OR PARDOT_TABLE.AE_Name IS NOT NULL
    OR DTR_TABLE.AE_Name IS NOT NULL
    OR MC_DATA_TABLE.AE_Name IS NOT NULL
    OR ECS_TABLE.AE_Name IS NOT NULL
    OR BDR_TABLE.AE_Name IS NOT NULL
    OR MC_BDR.AE_Name IS NOT NULL
    OR MCandPardot.AE_Name IS NOT NULL
    OR InteractionS.AE_Name IS NOT NULL
    OR B2BTable.AE_Name IS NOT NULL
    OR B2CTable.AE_Name IS NOT NULL
    OR CMRCTable.AE_Name IS NOT NULL
    OR B2CBTatble.AE_Name IS NOT NULL
    OR CSGAPITable.AE_Name IS NOT NULL
    OR CSGAP.AE_Name IS NOT NULL
    OR CSGRMOCTable.AE_Name IS NOT NULL
    OR CSGSATable.AE_Name IS NOT NULL
    OR CSGSMTable.AE_Name IS NOT NULL
    OR COMMCTable.AE_Name IS NOT NULL
    OR COMMCBTable.AE_Name IS NOT NULL
    OR GAMTable.AE_Name IS NOT NULL
    OR INDTable.AE_Name IS NOT NULL
    OR MAPTable.AE_Name IS NOT NULL
    OR MADTable.AE_Name IS NOT NULL
    OR MCAETable.AE_Name IS NOT NULL
    OR PCATable.AE_Name IS NOT NULL
    OR PLATFTable.AE_Name IS NOT NULL
    OR QUIPTable.AE_Name IS NOT NULL
    OR QUIPBTable.AE_Name IS NOT NULL
    OR TAETable.AE_Name IS NOT NULL
    OR TABLEAUTable.AE_Name IS NOT NULL
    OR TRAILHEADTable.AE_Name IS NOT NULL
)
