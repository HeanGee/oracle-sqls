with
    ACCTable as (
        select
            AccountT.ID_15  AccountID
        from  ODS.ODS_ACCOUNT AccountT
        where AccountT.DEL_FLG = 'N'
        and AccountT.ID in (select AccountDetails.ACCOUNTID from ODS.ODS_ACCOUNT_DETAIL__C AccountDetails where DEL_FLG = 'N')
        and AccountT.ID_15 in (select AccountIntelligence.ACCOUNT__C from ODS.ODS_ACCOUNT_INTELLIGENCE__C AccountIntelligence where DEL_FLG = 'N')
        ),
AETable as (
       select
        Teamtable.SFBASE__ACCOUNT__C account_id,
        TableAE.Name AE_Name,
        TeamRole.Name AE_Role
    from ODS.ODS_SFBASE__SALESFORCETEAM__C Teamtable
    inner join ACCTable
    on  ACCTable.AccountID = Teamtable.SFBASE__ACCOUNT__C
    left join ODS.ODS_USER TableAE
        on TableAE.ID_15 = Teamtable.sfbase__user__c
    left join ODS.ODS_TEAM_ROLE__C TeamRole
        on Teamtable.TEAM_ROLE__C = TeamRole.ID_15
    where TeamRole.Name in ('B2B Commerce AE', 'B2C Commerce AE', 'B2C Commerce BDR', 'CSG Account Partner - Industries',
     'CSG Account Partner', 'CSG Renewals Mgr Off-Cycle', 'CSG Solution Advisor', 'CSG Success Mgr', 'Commerce Cloud AE',
     'Commerce Cloud BDR', 'GAM', 'Industries AE', 'Interaction Studio AE', 'Map AE', 'MuleSoft Account Development',
     'MuleSoft Composer AE', 'Philanthropy Cloud AE', 'Platform AE', 'Quip AE', 'Quip BDR', 'Salesperson', 'TAE',
     'Tableau AE', 'myTrailhead AE')
        and Teamtable.SFBASE__ENDDATE__C is null
        and Teamtable.TERRITORY__C is not null
        and Teamtable.ISDELETED <> 'true'
        and Teamtable.DEL_FLG = 'N'
)
select
    ACCTable.AccountID AccountID, 
    B2BTable.AE_Name  B2B_AE,
    B2CTable.AE_Name  B2C_AE,
    CMRCTable.AE_Name CMRC_AE,
    B2CBTatble.AE_Name B2C_BDR_AE,
    CSGAPITable.AE_Name GSG_ACC_PART_IND_AE,
    CSGAP.AE_Name CSG_ACC_PART_AE,
    CSGRMOCTable.AE_Name CSG_RENEW_MGR_OFFC_AE,
    CSGSATable.AE_Name CSG_SOL_ADV_AE,
    CSGSMTable.AE_Name CSG_SUCC_MRG_AE,
    COMMCTable.AE_Name COMM_CL_AE,
    COMMCBTable.AE_Name COMM_CL_BDR_AE,
    GAMTable.AE_Name GAM_AE,
    INDTable.AE_Name INDU_AE,
    ISTable.AE_Name INT_STUD_AE,
    MAPTable.AE_Name MAP_AE,
    MADTable.AE_Name MULESOFT_ACC_DEV_AE,
    MCAETable.AE_Name MULESOFT_COMP_AE,
    PCATable.AE_Name PHILANTROPIC_AE,
    PLATFTable.AE_Name PLATFORM_AE,
    QUIPTable.AE_Name QUIP_AE,
    QUIPBTable.AE_Name QUIP_BDR_AE,
    SALESPTable.AE_Name SALESPERSON_AE,
    TAETable.AE_Name TAE_AE,
    TABLEAUTable.AE_Name TABLEAU_AE,
    TRAILHEADTable.AE_Name TRAILHEAD_AE

from ACCTable

left join AETable B2BTable
on ACCTable.AccountID = B2BTable.account_id and B2BTable.AE_Role = 'B2B Commerce AE'
left join AETable B2CTable
on ACCTable.AccountID = B2CTable.account_id and B2CTable.AE_Role = 'B2C Commerce AE'
left join AETable B2CBTatble
on ACCTable.AccountID = B2CBTatble.account_id and B2CBTatble.AE_Role = 'B2C Commerce BDR'
left join AETable CMRCTable
on ACCTable.AccountID = CMRCTable.account_id and CMRCTable.AE_Role = 'Commerce Cloud AE'
left join AETable CSGAPITable
on ACCTable.AccountID = CSGAPITable.account_id and CSGAPITable.AE_Role = 'CSG Account Partner - Industries'
left join AETable CSGAP
on ACCTable.AccountID = CSGAP.account_id and CSGAP.AE_Role = 'CSG Account Partner'
left join AETable CSGRMOCTable
on ACCTable.AccountID = CSGRMOCTable.account_id and CSGRMOCTable.AE_Role = 'CSG Renewals Mgr Off-Cycle'
left join AETable CSGSATable
on ACCTable.AccountID = CSGSATable.account_id and CSGSATable.AE_Role = 'CSG Solution Advisor'
left join AETable CSGSMTable
on ACCTable.AccountID = CSGSMTable.account_id and CSGSMTable.AE_Role = 'CSG Success Mgr'
left join AETable COMMCTable
on ACCTable.AccountID = COMMCTable.account_id and COMMCTable.AE_Role = 'Commerce Cloud AE'
left join AETable COMMCBTable
on ACCTable.AccountID = COMMCBTable.account_id and COMMCBTable.AE_Role = 'Commerce Cloud BDR'
left join AETable GAMTable
on ACCTable.AccountID = GAMTable.account_id and GAMTable.AE_Role = 'GAM'
left join AETable INDTable
on ACCTable.AccountID = INDTable.account_id and INDTable.AE_Role = 'Industries AE'
left join AETable ISTable
on ACCTable.AccountID = ISTable.account_id and ISTable.AE_Role = 'Interaction Studio AE'
left join AETable MAPTable
on ACCTable.AccountID = MAPTable.account_id and MAPTable.AE_Role = 'Map AE'
left join AETable MADTable
on ACCTable.AccountID = MADTable.account_id and MADTable.AE_Role = 'MuleSoft Account Development'
left join AETable MCAETable
on ACCTable.AccountID = MCAETable.account_id and MCAETable.AE_Role = 'MuleSoft Composer AE'
left join AETable PCATable
on ACCTable.AccountID = PCATable.account_id and PCATable.AE_Role = 'Philanthropy Cloud AE'
left join AETable PLATFTable
on ACCTable.AccountID = PLATFTable.account_id and PLATFTable.AE_Role = 'Platform AE'
left join AETable QUIPTable
on ACCTable.AccountID = QUIPTable.account_id and QUIPTable.AE_Role = 'Quip AE'
left join AETable QUIPBTable
on ACCTable.AccountID = QUIPBTable.account_id and QUIPBTable.AE_Role = 'Quip BDR'
left join AETable SALESPTable
on ACCTable.AccountID = SALESPTable.account_id and SALESPTable.AE_Role = 'Salesperson'
left join AETable TAETable
on ACCTable.AccountID = TAETable.account_id and TAETable.AE_Role = 'TAE'
left join AETable TABLEAUTable
on ACCTable.AccountID = TABLEAUTable.account_id and TABLEAUTable.AE_Role = 'Tableau AE'
left join AETable TRAILHEADTable
on ACCTable.AccountID = TRAILHEADTable.account_id and TRAILHEADTable.AE_Role = 'myTrailhead AE'

where (B2BTable.AE_Name is not null or B2CTable.AE_Name is not null or CMRCTable.AE_Name is not null or
 B2CBTatble.AE_Name is not null or CSGAPITable.AE_Name is not null or CSGAP.AE_Name is not null or
 CSGRMOCTable.AE_Name is not null or CSGSATable.AE_Name is not null or CSGSMTable.AE_Name is not null or
 COMMCTable.AE_Name is not null or COMMCBTable.AE_Name is not null or GAMTable.AE_Name is not null or
 INDTable.AE_Name is not null or ISTable.AE_Name is not null or MAPTable.AE_Name is not null or
 MADTable.AE_Name is not null or MCAETable.AE_Name is not null or PCATable.AE_Name is not null or
 PLATFTable.AE_Name is not null or QUIPTable.AE_Name is not null or QUIPBTable.AE_Name is not null or
 SALESPTable.AE_Name is not null or TAETable.AE_Name is not null or TABLEAUTable.AE_Name is not null or
 TRAILHEADTable.AE_Name is not null)
