SELECT
    AccountT.COMPANY__C AS "Company ID",
    SUM(accountT.LOCKED_EMPLOYEES__C) AS "Emp Count"
FROM
    ODS.ODS_ACCOUNT AccountT
WHERE
    AccountT.DEL_FLG LIKE 'N'
    AND AccountT.COMPANY__C IS NOT null
GROUP BY
    AccountT.COMPANY__C
