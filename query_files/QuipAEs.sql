WITH
    ACCTable AS (
        SELECT 
            AccountT.ID_15  AccountID
        FROM ODS.ODS_ACCOUNT AccountT 
        WHERE AccountT.DEL_FLG = 'N'
            AND AccountT.ID IN (SELECT AccountDetails.ACCOUNTID FROM ODS.ODS_ACCOUNT_DETAIL__C AccountDetails WHERE DEL_FLG = 'N')
            AND AccountT.ID_15 IN (SELECT AccountIntelligence.ACCOUNT__C FROM ODS.ODS_ACCOUNT_INTELLIGENCE__C AccountIntelligence WHERE DEL_FLG = 'N')
    ),

    AETable AS (
        SELECT
            Teamtable.SFBASE__ACCOUNT__C account_id,
            TableAE.Name AE_Name,
            TableAEM.name AE_Manager
        FROM ODS.ODS_SFBASE__SALESFORCETEAM__C Teamtable
            -- Limits the accounts to those for AccountIntelligence.
            INNER JOIN ACCTable
            ON  ACCTable.AccountID = Teamtable.SFBASE__ACCOUNT__C

            -- Getting Account Executives.
            LEFT JOIN ODS.ODS_USER TableAE
            ON TableAE.ID_15 = Teamtable.sfbase__user__c

            -- Getting Managers of AEs.
            LEFT JOIN ODS.ODS_USER TableAEM
            ON TableAEM.ID = TableAE.managerid

            -- Getting Managers of AEs Managers.
            --LEFT JOIN ODS.ODS_USER ManagerOfAEManager
            --ON ManagerOfAEManager.ID = TableAEM.managerid

            -- Getting Roles.
            LEFT JOIN ODS.ODS_TEAM_ROLE__C TeamRole
            ON TeamRole.ID_15 = Teamtable.TEAM_ROLE__C
            
        WHERE
            (
                TeamRole.Name like '%ECS%'
            )
            AND Teamtable.SFBASE__ENDDATE__C is null
            AND Teamtable.TERRITORY__C is not null
            AND Teamtable.ISDELETED <> 'true' 
            AND Teamtable.DEL_FLG = 'N'
    )

SELECT
    ACCTable.AccountID AccountID,
    ECS_TABLE.AE_Name QUIP_ECS_AE,
    ECS_TABLE.AE_Manager QUIP_ECS_MANAGER
    --ECS_TABLE.AE_Role Role
    --ECS_TABLE.AE_Manager_Manager ECS_MANAGER_M
FROM ACCTable
    
    LEFT JOIN AETable ECS_TABLE
    ON ACCTable.AccountID = ECS_TABLE.account_id
    
WHERE (
    ECS_TABLE.AE_Name IS NOT NULL
)
ORDER BY ECS_TABLE.AE_Name asc