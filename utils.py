import threading
import time
from contextlib import contextmanager
from os import system

import numpy as np
import pandas as pd
from blessings import Terminal as Cmd


class _Colors:
    def __init__(self):
        """This class is only for holding color properties"""
        pass

    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    BLACK = '\u001b[30m'
    RED = '\u001b[31m'
    GREEN = '\u001b[32m'
    YELLOW = '\u001b[33m'
    BLUE = '\u001b[34m'
    MAGENTA = '\u001b[35m'
    CYAN = '\u001b[36m'
    WHITE = '\u001b[37m'
    RESET = '\u001b[0m'


lock = msg_len = None
progress_bar_success: threading.Event = None
progress_bar_unsuccess: threading.Event = None


def decorate_with(text="", decorator=_Colors.RESET):
    return f"{decorator}{text}{_Colors.RESET}"


def move_cur_right(position):
    return f"\u001b[{position + 1}C"


class ProgressBarVars:
    def __init__(self):
        self.progress_bar_success: threading.Event = None
        self.progress_bar_unsuccess: threading.Event = None
        self.msg_len = None


@contextmanager
def progress_bar(prefix="", repeater="*", forever=False):
    pgv = ProgressBarVars()

    pgv.msg_len = 0
    exception = None
    pgv.progress_bar_success = threading.Event()
    pgv.progress_bar_unsuccess = threading.Event()

    th = threading.Thread(target=__stdout__, kwargs={'prefix': prefix, 'repeater': repeater, 'pgv': pgv})
    start = time.time()
    th.start()

    try:
        yield
        pgv.progress_bar_success.set()
    except BaseException as e:
        pgv.progress_bar_unsuccess.set()
        exception = e
    finally:
        th.join()

        print(f"\r "
              f"{move_cur_right(pgv.msg_len + 1)}"
              f"{decorate_with(text=str(time.time() - start), decorator=_Colors.RESET)}")

    if exception:
        raise exception


def __stdout__(prefix="", repeater="*", suffix="", pgv: ProgressBarVars = None):
    print(f"{decorate_with(text=prefix, decorator=_Colors.RESET)}", end='\r')
    pgv.msg_len = len(prefix)
    while not (pgv.progress_bar_success.isSet() or pgv.progress_bar_unsuccess.isSet()):
        for i in range(0, 100):
            time.sleep(0.015625)
            width = (i + 1) // 4
            fill = f"{repeater * width}{' ' * (25 - width)}{suffix}"
            bar = f"{move_cur_right(pgv.msg_len)}{decorate_with(text=fill, decorator=_Colors.YELLOW)}"
            print(f"\r{bar}", end='\r')

    # Complete the line with full repeater and append [OK] or [ERROR]
    pgv.msg_len = len(prefix) + 25

    bar = f"{move_cur_right(len(prefix))}{decorate_with(repeater * 25, _Colors.YELLOW)}"
    end = 'ERROR' if pgv.progress_bar_unsuccess.isSet() else 'OK'
    end_decoration = _Colors.RED if pgv.progress_bar_unsuccess.isSet() else _Colors.YELLOW
    pgv.msg_len += 2 + len(end)  # [OK] or [ERROR]

    bar += f" {decorate_with('[', _Colors.RESET)}" \
           f"{decorate_with(end, end_decoration)}" \
           f"{decorate_with(']', _Colors.RESET)}"
    print(f"\r{bar}", end='\r')


# Add new fields into output
def set_output_fields(play_field, play, play_acv_field, play_acv, row, headers):
    headers.append(play_field) if play_field and play_field not in headers else None
    headers.append(play_acv_field) if play_acv_field and play_acv_field not in headers else None

    if play_field:
        row[play_field] = play
    if play_acv_field:
        row[play_acv_field] = play_acv


# Add new fields into output
def set_output_fields_v2(row=None, rows=None, extension=None, play_field=None, play=None, index=None):
    if play_field not in extension.keys():
        extension[play_field] = list(np.full(rows, ""))
    extension[play_field][index] = play


# Add new fields into output
def set_output_fields_v3(df: pd.DataFrame = None, field=None):
    extension = {}
    if field not in df.columns:
        extension[field] = list(np.full(len(df.index), ""))
        df = df.join(pd.DataFrame(extension, index=df.index))
    return df


# Add fields from hls
def add_fields_from_hls(hls, row, headers, fields=None):
    if hls is None or fields is None:
        return

    # Detect whether one or more fields doesn't exist in HLS
    non_existent_count = [field for field in fields if field not in list(hls.keys())]

    # Raise Exception if any specified required field doesn't exist in HLS
    raise_exception(f"<{', '.join(non_existent_count)}> field{'s' if len(non_existent_count) > 1 else ''} "
                    f"doesn't exist in HLS.") if non_existent_count else None

    # Copy required HLS fields into CSV output, using python List-Comprehension.
    [set_output_fields(None, None, field, hls[field], row, headers) for field in fields]


def raise_exception(msg, exception_class=None):
    raise (Exception(msg) if not exception_class else exception_class(msg))


def print_at(msg="Default Msg", row=1):
    trm = Cmd()
    with trm.location(0, row):
        print("\x1b[?25l\033[2K\033[1G  " + msg, flush=True)  # Hide prompt blink


class ProgressBarWithTime(threading.Thread):
    def __init__(self):
        super().__init__()
        self.progress_bar_success: threading.Event = None
        self.progress_bar_unsuccess: threading.Event = None
        self.msg_len = None
        self.line = None
        self.sec = 0
        self.min = 0
        self.hour = 0
        self.time = 0

    def initialize_time(self):
        self.time = time.time() if not self.time else self.time

    def get_time(self):
        self.initialize_time()

        self.sec = time.time() - self.time
        self.hour = int(0 if self.sec < 3600 else self.sec // 3600)

        self.sec -= self.hour * 3600
        self.min = int(0 if self.sec < 60 else self.sec // 60)

        self.sec -= self.min * 60
        self.sec = int(self.sec)

        return f"{'' if self.hour > 9 else '0'}{self.hour}:" \
               f"{'' if self.min > 9 else '0'}{self.min}:" \
               f"{'' if self.sec > 9 else '0'}{self.sec}"


@contextmanager
def progress_bar_with_time(prefix="", line=0, clear=True, last=False):
    pgv = ProgressBarWithTime()
    pgv.msg_len = 0
    pgv.progress_bar_success = threading.Event()
    pgv.progress_bar_unsuccess = threading.Event()
    pgv.line = line

    exception = None

    pbth = threading.Thread(target=progress_bar_printer, kwargs={'prefix': prefix, 'pgv': pgv})
    system('clear') if clear else None
    pbth.start()

    try:
        yield
        pgv.progress_bar_success.set()
    except BaseException as e:
        pgv.progress_bar_unsuccess.set()
        exception = e
    except KeyboardInterrupt as kbi:
        print_at("KEYBOARD INTERRUTION !!!!!!!!!!", pgv.line)
        exception = kbi
    finally:
        pbth.join()

    print('\n' * line) if last else None

    if exception:
        raise exception

    return pbth


def progress_bar_printer(prefix="", pgv: ProgressBarWithTime = None):
    while not (pgv.progress_bar_success.isSet() or pgv.progress_bar_unsuccess.isSet()):
        time.sleep(0.5)
        bar = f"{decorate_with(text=pgv.get_time(), decorator=_Colors.YELLOW)} " \
              f"{decorate_with(text=prefix, decorator=_Colors.RESET)}"
        print_at(f" {bar}", pgv.line)

    bar = f"{decorate_with(pgv.get_time(), _Colors.YELLOW)}"
    end = 'ERROR' if pgv.progress_bar_unsuccess.isSet() else 'OK'
    end_decoration = _Colors.RED if pgv.progress_bar_unsuccess.isSet() else _Colors.GREEN
    status = f"{decorate_with('[', _Colors.RESET)}" \
             f"{decorate_with(end, end_decoration)}" \
             f"{decorate_with(']', _Colors.RESET)}"

    bar = f"{bar} {status} {decorate_with(text=prefix, decorator=_Colors.RESET)}"
    print_at(f" {bar}", pgv.line)


def seconds_to_clock(seconds=0):
    if seconds > 3600:
        hours = int(seconds // 3600)
        seconds -= hours * 3600
        mins = int(seconds // 60)
        seconds -= mins * 60
    elif seconds > 60:
        hours = 0
        mins = int(seconds // 60)
        seconds -= mins * 60
    else:
        hours = mins = 0

    seconds = int(seconds)
    return f"{'0' if hours < 10 else ''}{hours}:" \
           f"{'0' if mins < 10 else ''}{mins}:" \
           f"{'0' if seconds < 10 else ''}{seconds}"


def calculate_segment(company_employees_count):
    if company_employees_count >= 4500:
        segment = 'ENTR'  # 1000 or more employees, now 4500+
    elif company_employees_count >= 200:
        segment = 'CMRCL'  # Between 200 and 999 employees Now 200 to 4500
    else:
        segment = 'SMB'
    return segment


# Narrow value between a limits
def limit_value(acv, minimum, maximum):
    (minimum, maximum) = (maximum, minimum) if minimum > maximum else (minimum, maximum)

    return min([max([acv, minimum]), maximum])


# Return list of fields possibly of int type
def get_numeric_field(obj) -> list:
    import csv
    import re

    fields = []
    number_field_regex = re.compile(
        r'\(converted\)$|\(Converted\)$|\(Numeric\)$|\(Num\)$|PTB Seg|Order Q|Ord Q|Qty|AOV|(?!\# Contacts_y)\#|\ '
        r'ACV$|CREDs / User|\ Score$|^Number of ')

    if isinstance(obj, csv.DictReader):
        fields = obj.fieldnames

    if isinstance(obj, pd.DataFrame):
        fields = list(obj.columns)

    int_fields = [field for field in fields if number_field_regex.search(field)]
    int_fields.sort()

    return int_fields
